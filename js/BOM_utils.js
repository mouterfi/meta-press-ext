// SPDX-FileName: ./BOM_utils.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

// BOM stands for Browser Object Model
// function defined here have dependancies to in-browser JavaScript API

import { urlify } from './js_utils.js'

export const	$ = (str) => document.querySelector(str)
export const $$ = (str) => document.querySelectorAll(str)
export const id = (str) => document.getElementById(str)
/**
 * Redirects console printing to an alert displayed on demand
 * Used in mobile debugging
 */
export function alert_console() {
	let log = ''
	function make_mp_print (name, nat_obj) {	// eslint-disable-line no-inner-declarations
		return function () {
			log += `${name} : `
			nat_obj.apply(console, arguments)
			for (let i=0; i < arguments.length; i++)
				log += ' '+ String(arguments[i])
			log += '\n'
		}
	}
	console.info = make_mp_print('Info', console.info)
	console.log = make_mp_print('Log', console.log)
	console.warn = make_mp_print('Warning', console.warn)
	console.error = make_mp_print('Error', console.error)
	console.trace = make_mp_print('Trace', console.trace)
	let btn = id('mp_dev')
	btn.style.display = 'block'
	btn.addEventListener('click', () => alert(log))
}
/**
 * Decode the HTML entities from the given string to their UTF8 counterparts
 * More info : https://stackoverflow.com/questions/3700326/decode-amp-back-to-in-javascript
 * @example htmlDecode("&lt;img src='myimage.jpg'&gt;") returns "<img src='myimage.jpg'>"
 * @param {string} s The string to decode
 * @returns {string} The decoded string
 */
export function HTML_decode_entities (s) {
	let elt = document.createElement('textarea')
	// console.log('input of HTML_decode_entities', s)
	elt.innerHTML = s
	// console.log('elt_value', elt.value)
	return elt.value || ''
}
/**
 * Check if the browser is in dark mode.
 * https://stackoverflow.com/questions/50840168/how-to-detect-if-the-os-is-in-dark-mode-in-browsers
 * @returns {boolean} dark mode
 */
export function isDarkMode() {
	return window.matchMedia('(prefers-color-scheme: dark)').matches
}
/**
 * Upload the data in a filename and submit to the user.
 * @param {string} file_name The filename
 * @param {string} str The data to upload
 */
export function upload_file_to_user(file_name, str) {
	let a = document.createElement('a')
	a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(str)}`
	a.download=file_name
	a.click()
}
/**
 * Return all the months name of the given locale in the given format
 * From https://stackoverflow.com/a/70869819
 * @param {object} with properties : locale (fr, en, ja…) ; format (short, long, numeric…)
 * @returns {Array} an array containing the 12 months names
 */
export function get_months(locale = navigator.language, format = 'long') {
	const locale_format = new Intl.DateTimeFormat(locale, {month: format}).format
	return [...Array(12).keys()].map((m) => locale_format(new Date(2021, m)))
}
/**
 * Lower case, normalize and remove diacritics from the given string
 * https://stackoverflow.com/a/51874002
 */
function norm_str (str) {
	return str.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f.]/g, '')
}
/**
 *
 */
function string_index_in_array(str, arr) {
	return arr.findIndex(a => norm_str(a).startsWith(norm_str(str)))
	// return arr.findIndex(a => norm_str(a).search(norm_str(str)) > 0)
}
/** Gets the locale that the browser is asking for */
export const get_browser_locale = () => navigator.language || navigator.userLanguage
/**
 * Return the month number of the given month name from the given language
 * @param {string} a locale (fr, en, ja…)
 * @param {string} a month name in the given language (février, january, 5月)
 * @returns {number} the corresponding number (2, 1, 5) or the given month_name if not matched
 */
export function lang_month_nb(lang, month_name) {
	let a = string_index_in_array(month_name, get_months(lang)) + 1  // months start at 0 in JS
	if (a > 0) return a  // nl maart is abbr-ed in mrt. so :
	a = string_index_in_array(month_name, get_months(lang, 'short')) + 1
	if (a > 0) return a
	return month_name
}
/**
 * Count the number of browser supported locales as per ISO 639-2 2-letters codes
 * 2022-05-12 : 218 in Firefox 102.0a1
 */
export function locales() {
	return supported_ISO_639_2_language_locales()
}
export function supported_ISO_639_2_language_locales() {
	let a = new Set()
	let b = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
		'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let b_ = ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
		'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let r
	for (let c of b)
		for (let d of b)
			for (let e of b_) {
				r = Intl.DateTimeFormat.supportedLocalesOf([c+d+e])
				r.length && a.add(r[0])
			}
	return Array.from(a).sort()
}
function supported_xx_xx_country_locales() {	// eslint-disable-line no-unused-vars
	let a = new Set()
	let b = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
		'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let r
	for (let c of b)
		for (let d of b)
			for (let e of b)
				for (let f of b) {
					r = Intl.DateTimeFormat.supportedLocalesOf([`${c+d}-${e+f}`])
					r.length && a.add(r[0])
				}
	return Array.from(a).sort()
}
export function known_countries_and_code(lang) {
	let a = {}
	let b = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q',
		'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let b_ = ['', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
		'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
	let k
	let x = new Intl.DisplayNames([lang], {type: 'region'})
	for (let c of b)
		for (let d of b)
			for (let e of b_) {
				k = c+d+e
				try {
					a[k] = x.of([k])
				} catch (err) {
					continue
				}
			}
	// return Array.from(a).sort()
	return a
}
/**
 * Find the favicon with the given HTML fragment and domain.
 * @param {Node} html_fragment The working area
 * @param {string} domain_part The domain of the website
 * @returns The favicon url
 */
export function get_favicon_URL(HTML_fragment, domain_part) { // favicon may be implicit
	let fav_node = HTML_fragment.querySelector('link[rel~="icon"]')
	return urlify(fav_node && fav_node.attributes.href.value || '/favicon.ico', domain_part)
}
/*
 *
 */
export function get_HTML_lang(HTML_fragment) {  // lang could be find in
	// or attributed <html … lang= -> split(/[-_]/)
	// <meta http-equiv="Content-language" content="fr">
	// <meta property="og:locale" content="fr_FR">
	let lang = HTML_fragment.querySelector('html').lang
	if (!lang) {
		let node = HTML_fragment.querySelector('meta[http-equiv="Content-Language"]') ||
			HTML_fragment.querySelector('meta[property="og:locale"]')
		lang = node.content || ''
	}
	let lang_split = lang.split(/[_-]/)
	return [lang_split[0], lang_split[1]]
}
/**
 * Find the RSS feed of a given HTML fragment and domain.
 * @param {Node} html_fragment The working area
 * @param {string} domain_part The domain of the website
 * @returns The favicon url
 */
export function get_RSS_URL(html_fragment, domain_part) { // favicon may be implicit
	let rss = html_fragment.querySelector('link[rel~="alternate"][type="application/rss+xml"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	rss = html_fragment.querySelector('a[data-smarttag-name="rss"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	rss = html_fragment.querySelector('a[aria-label="RSS"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	rss = html_fragment.querySelector('a[href*="feed"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	rss = html_fragment.querySelector('a[href*="rss"]')
	if (rss) return urlify(rss.attributes.href.value, domain_part)
	for (let a of html_fragment.querySelectorAll('a')) {
		if (a.textContent.includes('feed') ||
			a.textContent.includes('rss') ||
			a.textContent.includes('RSS')
		)
			return urlify(rss.attributes.href.value, domain_part)
	}
	if (!rss)
		console.log('RSS not found', domain_part, html_fragment)
	return rss && urlify(rss.attributes.href.value, domain_part) || ''
}

/**
 * Return a namespace URL from a given prefix. Contains all the namespaces we found yet.
 * Extended version of : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_using_XPath_in_JavaScript#Implementing_a_User_Defined_Namespace_Resolver
 * @param {string} prefix The prefix
 * @returns {string} the corresponding URL of the intended prefix norm
 */
function NS_resolver(prefix) {
	const NS = {
		'atom': 'http://www.w3.org/2005/Atom',
		'cc': 'http://web.resource.org/cc/',
		'content':'http://purl.org/rss/1.0/modules/content/',
		'dc': 'http://purl.org/dc/elements/1.1/',
		'enc': 'http://purl.oclc.org/net/rss_2.0/enc/',
		'image': 'http://www.google.com/schemas/sitemap-image/1.1',
		'itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd',
		'mathml': 'http://www.w3.org/1998/Math/MathML',
		'media': 'http://search.yahoo.com/mrss/',
		'prism': 'http://prismstandard.org/namespaces/basic/2.0/',
		'rdf' : 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
		'slash': 'http://purl.org/rss/1.0/modules/slash/',
		'sy': 'http://purl.org/rss/1.0/modules/syndication/',
		'wfw': 'http://wellformedweb.org/CommentAPI/',
		'xhtml' : 'http://www.w3.org/1999/xhtml',
	}
	return NS[prefix] || null
}

// Evaluate an XPath expression aExpression against a given DOM node
// or Document object (aNode), returning the results as an array
// thanks wanderingstan at morethanwarm dot mail dot com for the
// initial work.
/**
 * The XPath evaluator used by {@link evaluateXPath}.
 * @constant
 * @type {XPathEvaluator}
 */
const xpe = new XPathEvaluator()
/**
 * Search the XPath expression path in a node (querySelectorAll for XPath).
 * @param {Node} node The node working area
 * @param {string} XPath_expression The xpath expression
 * @returns {Array} All the found matchs
 */
export function evaluateXPath(node, XPath_expression) {
	// let nsResolver = document.createNSResolver(  // 2023-07-07: not working with RSS files
	// 	node.ownerDocument == null ? node.documentElement : node.ownerDocument.documentElement)
	let result = xpe.evaluate(XPath_expression, node, NS_resolver, XPathResult.ANY_TYPE, null)
	let found = [], res
	// console.info ('XPath result', result)
	/*if (typeof(result) === 'string')
		return result*/
	while (res = result.iterateNext())
		found.push(res)
	if (found.length === 1)
		found = found[0]
	return found
}
/**
 * Creates a style link with href or a style node.
 * https://stackoverflow.com/a/524798
 * @param {string} href The stylesheet to reach if undefined create style
 * @returns {StyleSheet} The created or loaded stylesheet
 */
export function createStyleSheet(href) {
	let elt
	if(typeof href !== 'undefined') {
		elt = document.createElement('link')
		elt.type = 'text/css'
		elt.rel = 'stylesheet'
		elt.href = href
	} else elt = document.createElement('style')
	// elt['generated'] = true
	document.getElementsByTagName('head')[0].appendChild(elt)
	let sheet = document.styleSheets[document.styleSheets.length - 1]
	// if(typeof sheet.addRule === 'undefined') // we're not targeting browsers without addRule()
	//	sheet.addRule = addRule
	if(typeof sheet.removeRule === 'undefined')
		sheet.removeRule = sheet.deleteRule
	return sheet
}
/**
 * Demand a file from the user and pass it to a hoop treatment function
 * @param {Function} treatment_hook Given function to act on the file content
 */
export function read_user_file(treatment_hook) {
	let input_elt = document.createElement('input')
	input_elt.type = 'file'
	input_elt.click()
	input_elt.addEventListener('change', () => {
		let file = input_elt.files[0]
		if (file) {
			let reader = new FileReader()
			reader.readAsText(file, 'UTF-8')
			reader.onload = treatment_hook
			reader.onerror = () => console.error('error importing user file')
		}
	}, false)
}
/**
 * Activate dropdown buttons constructs (.drop_btn / .drop_down_div)
 */
export function dropdown_alive() {
	let menus = document.querySelectorAll('.drop_btn')
	for (const i of menus) {
		i.onclick = evt => {
			let elt = evt.target
			elt.textContent = elt.classList.contains('drop_active') ? ' + ' : ' - '
			elt.classList.toggle('drop_active')
			elt.parentNode.querySelector('.drop_down_div').classList.toggle('drop_display')
		}
	}
}
/**
 * Set the language in the HTML document.
 * @param {string} lg The language
 */
export function set_HTML_lang(lg) { document.body.parentElement.lang = lg }
//export const intl_lang_name = (lg) => new Intl.DisplayNames([lg], {type: 'language'}).of(lg)
