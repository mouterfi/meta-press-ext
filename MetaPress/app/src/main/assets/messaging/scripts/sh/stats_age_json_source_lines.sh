#!/bin/bash

echo "Meta-Press.es"
echo "Computing the age of each line in json/sources.json"
echo "usage : ./scripts/sh/stats_age_json_source_lines.sh"

SRC_FILE="json/sources.json"
CUR_YEAR=`date +%Y`
GIT_BLAME=`git blame $SRC_FILE`
total_lines=0

for no_year in `seq 2019 $CUR_YEAR`; do
	nb_lines=`echo "$GIT_BLAME" | rg "\s$no_year-\d\d-\d\d \d\d:\d\d:\d\d" | wc -l`
	total_lines=$((total_lines + nb_lines))
	echo "$no_year $nb_lines"
done;

echo "total stat line number : $total_lines"
FILE_LINE_NB=`cat $SRC_FILE | wc -l`
echo "total file line number : $FILE_LINE_NB"
echo "delta (should be 0) : $(($total_lines - $FILE_LINE_NB))"
