# Télécharger et installer le logiciel android studio 
- vous pouvez télécharger android studio via le lien : https://android-studio.fr.uptodown.com/android/telecharger 
- si vous avez une machine assez puissante vous pouvez télécharger la dernière version, sinon penser à revenir vers les versions un peu plus anciennes

# importer le projet
 
- Télécharger le dossier MyApplication2 
- Sur android studio vous pouvez importer un nouveau projet : File /new / import project 

- Vous choisissez le dossier MyApplication2

# Exécuter le projet
Apres avoir importé le dossier deux fichiers s'ouvrent : 
- un fichier qui contient le code kotlin (MainActivity.kt)
- un fichier xml qui contient le design de l'application (activity_main.xml)
- vous cliquez sur run pour la compilation et l'exécution ou Maj+F10

# GeckoView

geckoview est déjà injecté dans l'application,
j'ai ajouté ces lignes dans le fichier build.gradle : 

```kotlin
ext {
    
    geckoview_channel = "nightly"

    geckoview_version = "70.0.20190712095934"
}
```
c'est pour la version de geckoview et de canale de publication.
ensuite dans le bloc **dependencies** ajouter ça : 
```
implementation "org.mozilla.geckoview:geckoview-${geckoview_channel}:${geckoview_version}"
```

aussi dans le bloc **repositories** ajouter : 
```
 maven {
            url "https://maven.mozilla.org/maven2/"
        }
```

Ensuite
dans le fichier xml (activity_main.xml) vous trouverez une balize **TextView** qui est utilisée pour afficher du texte dans une interface utilisateur Android,  on a pas besoin de ça donc on la supprime.

on crée une autre balise pour injecter geckoview : 
```
   <org.mozilla.geckoview.GeckoView
        xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools"
        android:id="@+id/geckoview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".MainActivity"/>
```
ce bloc de code XML définit une GeckoView qui s'étend sur toute la largeur et la hauteur de son parent  **LinearLayout**. L'ID geckoview peut être utilisé pour faire référence à cette vue dans le code Kotlin de l'activité MainActivity.

Enfin, maintenant on peut utiliser geckoview pour charger un **site web** sur une application android,
il suffit juste de coller l'url de site web que vous voulez mettre sous android.
avec cette ligne : 
```
        geckoSession.loadUri("https://firefox-source-docs.mozilla.org/mobile/android/geckoview/consumer/permissions.html") 
```

 on peut coller le site qu'on veut. par exemple moi j'ai pris le lien d'une  documentation sur la GeckoView.

voici le résultat quand on lance l'application: 

<div style="display: flex;">
    <img src="mark1.png" alt="Image 1" style="flex: 1;">    
</div>

# GeckoView et Extension Web
Notre objectif c'est de faire marcher l'extension MetaPress et non pas un site web, pour cela on peut utiliser la méthode  **WebExtensionController.installBuiltIn** de cette manière : 

```
runtime.getWebExtensionController()
  .installBuiltIn("resource://android/assets/messaging/")
  ```
  en remplaçant le dossier messaging avec notre extension qui se trouve dans le dossier **assets**.

  mais là on doit utiliser une version de geckoview plus récente, au début on a utilisé la version 70 (70.0.20190712095934) qui est sortie en 2019.

  pour faire fonctionner la méthode installBuiltIn() nous aurons besoin de la dérnière version qui est 127 (127.0.20240423214125) 

# Android Studio 2023
afin d'éviter tous conflits de version, nous allons installer la dernière version de logiciel android studio.
on peut l'obtenir via ce lien : 
https://developer.android.com/studio?hl=fr

mais dans cette version les choses sont différentes, dans les dernières versions d'Android Studio (à partir de la version 2022.2), le fichier activity_main.xml n'est plus créé par défaut pour les nouveaux projets Android.

La raison principale est l'introduction de Jetpack Compose, un framework d'interface utilisateur moderne pour Android. Jetpack Compose utilise une approche déclarative pour construire des interfaces utilisateur, éliminant ainsi le besoin de fichiers XML pour définir les mises en page.

A la place de activity_main.xml, vous définissez désormais l'interface utilisateur de votre activité principale en utilisant du code Kotlin dans la fonction **setContent**. Cette fonction utilise des composables Jetpack Compose pour construire l'arborescence de l'interface utilisateur de manière déclarative.

on va commencer à créer un nouveau projet en cliquant sur file -> new -> project 

un fichier en kotlin MainActivity.kt sera créé, il s'agit d'une application Android simple qui affiche le texte "hello Android !" à l'écran en utilisant Jetpack Compose pour une expérience de développement d'interface utilisateur moderne et déclarative.

j'ai essayé d'exécuter directement ça mais il y'a une erreur : 
<div style="display: flex;">
    <img src="erreur1.png" alt="Image 1" style="flex: 1;">    
</div>

que faire dans ce cas ? 

j'ai supprimé device qui était par défaut, et j'ai créé un autre comme suite : 

1. cliquer sur Create New Device
2. choisir la catégorie que vous voulez (phone,tablette,...) moi j'ai choisi phone
3. choisir le modèle (pixel fold,pixel 8 pro...)
4. ensuite next
5. choisir l'image de système (R,Q,tiramisu...)
6. ensuite next
7. choisir l'orientation (Portrait,Landscape)
8. cliquer sur finish

<div style="display: flex;">
    <img src="installer_sdk2.png" alt="Image 1" style="flex: 1;">    
</div>

dans device manager vous trouverez votre device que vous venez de créer, maintenant il reste qu'à ré-exécuter le projet.
et boom ça se lance 

<div style="display: flex;">
    <img src="sol2.png" alt="Image 1" style="flex: 1;">    
</div>

ok!

Maintenant la manière d'injecter geckoview dans notre nouvelle application android (MetaPress) est un peu différente, puisque on n'a pas les meme fichiers avec la version 2021.

d'abord on a un fichier libs.versions.toml qui est utilisé pour centraliser les versions des bibliothèques et des plugins dans un projet Gradle Kotlin. Cela permet de définir une seule fois les versions de ces éléments, puis de les référencer dans le fichier build.gradle.kts pour maintenir la cohérence et faciliter les mises à jour.

on va ajouter cette ligne a ce fichier en dessous de bloc [versions]: 
```
geckoviewNightly = "127.0.20240423214125"
```

ensuite dans le fichier build.gradle.kts on ajoute juste cette ligne : 
```
implementation (libs.geckoview.nightly)
```

on n'a pas besoin de déclarer deux variable pour channel et version comme on avait fait avant, au début j'ai fait ça mais un message de werrning apparait disant que tu n'a pas besoin de faire ça 

<div style="display: flex;">
    <img src="warn1.png" alt="Image 1" style="flex: 1;">    
</div>

dans le fichier settings.gradle ajouter cette ligne : 

```
maven {
            url = uri("https://maven.mozilla.org/maven2/")
        }
```

maintenat on peut utiliser geckoview. Mais quand j'ai essayé d'utiliser directement geckoview avec du code kotlin comme suite : 
```
                    GeckoWebView("https://firefox-source-docs.mozilla.org/mobile/android/geckoview/consumer/geckoview-quick-start.html")
```

l'application ça se lance pas, tout marche bien mais l'application au bout de 3 à 4 seconde elle se ferme avec ce message :
<div style="display: flex;">
    <img src="stoping.png" alt="Image 1" style="flex: 1;">    
</div>

quand je suis rentré à l'application dans l'émulateur, j'ai remarqué qu'il y'a un problème de permession, et dans ce cas il faut ajouter cette ligne dans le fichier **AndroidManifest.xml**

```
    <uses-permission android:name="android.permission.INTERNET" />
```

pour avoir les permession.

et quand on ré-exécute, voilà ça marche
<div style="display: flex;">
    <img src="marche.png" alt="Image 1" style="flex: 1;">    
</div>