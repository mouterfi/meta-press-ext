// SPDX-Filename: ./update_black_list.js
// SPDX-FileCopyrightText: 2022 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import * as _ from '/js/js_utils.js'
export function enumerate_sources(src_obj, bl_json) {
	// const intl_lang_name = (lg) => new Intl.DisplayNames([lg], {type: 'language'}).of(lg)
	const eng_lang_name		 = (lg) => new Intl.DisplayNames(['en'], {type:'language'}).of(lg)
	const eng_country_name = (lg) => new Intl.DisplayNames(['en'], {type: 'region'}).of(lg)
	// for (let src of src_list) {
	for (const [src_key, src] of Object.entries(src_obj)) {
		bl_json[src_key] = ''
		bl_json[`${src_key}/`] = ''
		try {
			bl_json[src.tags.name] = ''
			if (_.is(src.tags.lang)) {	// src of extends type can be missing tags
				bl_json[src.tags.lang] = ''
				bl_json[`[${src.tags.lang}]`] = ''
				bl_json[eng_lang_name(src.tags.lang)] = ''
			}
			if (_.is(src.tags.country)) {
				bl_json[src.tags.country] = ''
				bl_json[`[${src.tags.country}]`] = ''
				bl_json[eng_country_name(src.tags.country)] = ''
			}
		} catch (exc) {
			console.log(exc)
			console.log(src.tags)
		}
	}
	for (let i = Object.keys(src_obj).length; i--;) {
		bl_json[i] = ''  // chromium is adding ending-slash to href attributes
		bl_json[`(${i})`] = ''	// categories have no more than the total of sources
	}
	return bl_json
}
