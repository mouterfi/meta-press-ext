// SPDX-FileName: ./mp_utils.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import * as _ from './js_utils.js'
import {
	evaluateXPath,
	HTML_decode_entities,
	get_browser_locale,
	lang_month_nb
} from './BOM_utils.js'
import { strip_HTML_tags, DOM_parser, is_DOM_parsererror } from './DOM_parser.js'
import * as sµ from './source_utils.js'

//const dbg = true
const dbg = false

const EXCERPT_SIZE = 350  // only used in search_source

// const word_sep_re = '(^|[\\]\\[!"#$%&\'()*+,./\\:;<=>?@^_`{|}~-…«»’\\s]|$)'
// Read from: https://stackoverflow.com/q/7576945
// any kind of punctuation character (including international e.g. Chinese and Spanish
// punctuation)
// author: http://www.regular-expressions.info/unicode.html
// source: https://github.com/slevithan/xregexp/blob/41f4cd3fc0a8540c3c71969a0f81d1f00e9056a9/src/addons/unicode/unicode-categories.js#L142

// note: XRegExp unicode output taken from http://jsbin.com/uFiNeDOn/3/edit?js,console (see
// console.log), then converted back to JS escaped unicode here

// http://rishida.net/tools/conversion/, then tested on http://regexpal.com/
// suggested by: https://stackoverflow.com/a/7578937

// added: extra characters like "$", "\uFFE5" [yen symbol], "^", "+", "=" which are not
// considered punctuation in the XRegExp regex(they are currency or mathmatical characters)

// added: \u3000-\u303F Chinese Punctuation for good measure
const word_sep_re = '(^|["$&\'*+.?…«»’\\s\\uFFE5^=`~()<>{}\\[\\]|\\u3000-\\u303F!-#%-\\x2A,-/\\:;\\x3F@\\x5B-\\x5D_\\x7B\\u00A1\\u00A7\\u00AB\\u00B6\\u00B7\\u00BB\\u00BF\\u037E\\u0387\\u055A-\\u055F\\u0589\\u058A\\u05BE\\u05C0\\u05C3\\u05C6\\u05F3\\u05F4\\u0609\\u060A\\u060C\\u060D\\u061B\\u061E\\u061F\\u066A-\\u066D\\u06D4\\u0700-\\u070D\\u07F7-\\u07F9\\u0830-\\u083E\\u085E\\u0964\\u0965\\u0970\\u0AF0\\u0DF4\\u0E4F\\u0E5A\\u0E5B\\u0F04-\\u0F12\\u0F14\\u0F3A-\\u0F3D\\u0F85\\u0FD0-\\u0FD4\\u0FD9\\u0FDA\\u104A-\\u104F\\u10FB\\u1360-\\u1368\\u1400\\u166D\\u166E\\u169B\\u169C\\u16EB-\\u16ED\\u1735\\u1736\\u17D4-\\u17D6\\u17D8-\\u17DA\\u1800-\\u180A\\u1944\\u1945\\u1A1E\\u1A1F\\u1AA0-\\u1AA6\\u1AA8-\\u1AAD\\u1B5A-\\u1B60\\u1BFC-\\u1BFF\\u1C3B-\\u1C3F\\u1C7E\\u1C7F\\u1CC0-\\u1CC7\\u1CD3\\u2010-\\u2027\\u2030-\\u2043\\u2045-\\u2051\\u2053-\\u205E\\u207D\\u207E\\u208D\\u208E\\u2329\\u232A\\u2768-\\u2775\\u27C5\\u27C6\\u27E6-\\u27EF\\u2983-\\u2998\\u29D8-\\u29DB\\u29FC\\u29FD\\u2CF9-\\u2CFC\\u2CFE\\u2CFF\\u2D70\\u2E00-\\u2E2E\\u2E30-\\u2E3B\\u3001-\\u3003\\u3008-\\u3011\\u3014-\\u301F\\u3030\\u303D\\u30A0\\u30FB\\uA4FE\\uA4FF\\uA60D-\\uA60F\\uA673\\uA67E\\uA6F2-\\uA6F7\\uA874-\\uA877\\uA8CE\\uA8CF\\uA8F8-\\uA8FA\\uA92E\\uA92F\\uA95F\\uA9C1-\\uA9CD\\uA9DE\\uA9DF\\uAA5C-\\uAA5F\\uAADE\\uAADF\\uAAF0\\uAAF1\\uABEB\\uFD3E\\uFD3F\\uFE10-\\uFE19\\uFE30-\\uFE52\\uFE54-\\uFE61\\uFE63\\uFE68\\uFE6A\\uFE6B\\uFF01-\\uFF03\\uFF05-\\uFF0A\\uFF0C-\\uFF0F\\uFF1A\\uFF1B\\uFF1F\\uFF20\\uFF3B-\\uFF3D\\uFF3F\\uFF5B\\uFF5D\\uFF5F-\\uFF65]|$)' // eslint-disable-line max-len

export async function fetch_raw (url, method, headers, ctype, body,
	search_terms, MAX_RES_BY_SRC, token, abort_controller) {
	return await fetch(
		format_search_url(url, search_terms, MAX_RES_BY_SRC, null, token),
		{
			signal: abort_controller && abort_controller.signal || null,
			method: method || 'GET',
			headers: Object.assign(
				headers || {},
				{'Content-Type': ctype || 'application/x-www-form-urlencoded'},
			),
			body: body ? format_search_url(body, search_terms, MAX_RES_BY_SRC, ctype, token) : null
		}
	)
}
/**
 *
 */
export function format_search_url(search_url, terms, max_res_by_src, search_ctype, token='') {
	return search_url.replace(/{}/g, !search_ctype ? encodeURIComponent(terms) : terms)
		.replace(/{#}/g, max_res_by_src)
		.replace(/{T}/g, token)  // used by AfricaIntelligence, or ScienceDirect.com
		.replace(/{D}/g, new Date().toISOString())  // used by Mobilizon
		.replace(/{,}/g, terms.replace(/ /g, ',')) // used by sciencepresse.qc.ca
}
/**
 *
 */
export async function extract_rep (raw_rep, raw_rep_re, rep_charset, rep_type) {
	let rep
	let rep_ctype = raw_rep.headers.get('content-type')
	if (rep_ctype.includes('application/json') || 'JSON' === rep_type) {
		if(raw_rep_re) {  // jsonp_to_json_re
			let raw_txt = _.drop_escaped_quotes(_.triw(await raw_rep.text()))
			rep = JSON.parse(_.regextract(raw_rep_re[0], raw_txt, raw_rep_re[1]))
		} else {
			rep = await raw_rep.json()
		}
	} else {  // non JSON sources
		let txt_rep, arr_rep
		if (rep_charset) {
			arr_rep = await raw_rep.arrayBuffer()
			let text_decoder = new TextDecoder(rep_charset, {fatal: true})
			txt_rep = text_decoder.decode(arr_rep)
		} else {
			txt_rep = await raw_rep.text()
			if (raw_rep_re)
				txt_rep = _.regextract(raw_rep_re[0], txt_rep, raw_rep_re[1])
		}
		rep = DOM_parse_text_rep(txt_rep, rep_ctype)
		if (!rep) throw `dom parser error ${rep}`
	}
	return rep
}
/**
 *
 */
export function get_results (src_def, rep) {
	let n = src_def
	let fdgs = []
	if ('JSON' === n.type && !_.is(n.json_to_html)) {
		fdgs = _.is(n.results) ? $_('results', rep, n, 0, false) : rep
	} else {
		if (_.is(n.json_to_html)) {
			let m = _.deep_copy(n)
			m.type = 'JSON'
			rep = $_('json_to_html', rep, m, 0, false)
			rep = DOM_parse_text_rep(rep, 'text/html')
		}
		if (_.is(n.results))
			fdgs = rep.querySelectorAll(n.results)
		else if (_.is(n.results_xpath))
			fdgs = evaluateXPath(rep, n.results_xpath)
		else {
			fdgs = rep.querySelectorAll('item') // we need fdgs to auto-detect RSS variant
			if (!fdgs.length)
				fdgs = rep.querySelectorAll('entry')  // it's ATOM flavour fdgs
			if (!fdgs.length) {
				let err_msg = 'no source definition for "results"'
				if (n.type && n.type == 'XML')
					err_msg = 'No items, no entries, maybe no results'
				throw err_msg
			}
		}
	}
	return fdgs
}
/**
 * Request the source and scrap the results
 * @param {string} i The source key
 * @param {Object} n The source definition
 * @returns {Object} Search JSON-export for this source
 */
export async function search_source (i, n, search_terms, userLang, fetch_abort_controller,
	MAX_RES_BY_SRC=10, TEST_SRC=false) {
	// let n = source_objs[i]
	// dbg && console.debug('search_source source definition', i, n)
	let src_query_start_date = new Date()
	let token = ''
	let r = {'errors': [], 'findings': [], 'meta_findings': [], 'search_terms': search_terms}
	if (n.token) {
		let raw_pre
		try {
			raw_pre = await fetch_raw (  // token for {T} in format_search_URL
				n.token.token_url,
				n.token.token_method,
				n.token.token_headers,
				n.token.token_ctype,
				n.token.token_body,
				search_terms,
				MAX_RES_BY_SRC,
				'',
				fetch_abort_controller
			)
		} catch (exc) {
			r.errors.push(sµ.create_error(
				n, sµ.error_code.NETWORK_ERROR, exc, sµ.error_status.ERROR, 'test_src', {}, 1))
			return r
		}
		let rep_pre
		if (!raw_pre.ok) {
			r.errors.push(sµ.create_error(n, sµ.error_code.ERROR,
				`fetch not ok : ${raw_rep.status}`, sµ.error_status.ERROR, 'test_src', {}, 1))
			return r
		}
		try {
			rep_pre = await extract_rep(raw_pre,
				n.token.raw_pre_re,
				n.token.charset,
				n.token.token_type
			)
		} catch (exc) {
			r.errors.push(sµ.create_error(n, sµ.error_code.ERROR,
				exc, sµ.error_status.ERROR, 'test_src', {}, 'log'))
			return r
		}
		// console.debug({rep_pre})
		token = $_('token_sel', rep_pre, n.token, 0)
	}
	// end of token fetching
	let raw_rep
	try {
		raw_rep = await fetch_raw (
			n.search_url,
			n.method,
			n.search_headers,
			n.search_ctype,
			n.body,
			search_terms,
			MAX_RES_BY_SRC,
			token,
			fetch_abort_controller
		)
	} catch (exc) {
		r.errors.push(sµ.create_error(n, sµ.error_code.NETWORK_ERROR,
			exc, sµ.error_status.ERROR, 'test_src', {}, 1))
		return r
	}
	try {
		if (!raw_rep.ok) {
			r.errors.push(sµ.create_error(n, sµ.error_code.ERROR,
				`fetch not ok : ${raw_rep.status}`, sµ.error_status.ERROR, 'test_src', {}, 1))
			return r
		}
		let rep
		try {
			rep = await extract_rep(raw_rep, n.raw_rep_re, n.tags.charset, n.type)
		} catch (exc) {
			r.errors.push(sµ.create_error(n, sµ.error_code.ERROR,
				exc, sµ.error_status.ERROR, 'test_src', {}, 'log'))
			return r
		}
		let fdgs = []
		try {
			fdgs = get_results(n, rep)
		} catch (exc) {
			r.errors.push(sµ.create_error(n, sµ.error_code.ERROR,
				exc, sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
		}
		let fdgs_nb = fdgs ? fdgs.length : 0
		// dbg && console.debug('fdgs', fdgs_nb, fdgs)
		let max_fdgs = MAX_RES_BY_SRC  //!\ fail things when undef
		// dbg && console.debug('MAX_REs_BY_SRC', MAX_RES_BY_SRC)
		fdgs_nb = Math.min(fdgs_nb, max_fdgs)
		if (0 === fdgs_nb || isNaN(fdgs_nb)) {
			r.errors.push(sµ.create_error(n, sµ.error_code.NO_RESULT,
				'No results', sµ.error_status.WARNING, 'test_src', {}, 'log'))
			if (TEST_SRC)  // keep empty sources in the metaFindings to allow reports
				return r
		}
		let res_nb_str = ''
		if (n.type !== 'XML') {
			try { res_nb_str = $_('res_nb', rep, n)}
			catch (exc) {
				r.errors.push(sµ.create_error(n, sµ.error_code.ERROR,
					exc, sµ.error_status.ERROR, 'test_src', {}, 'log'))
			}
		} else {
			n = set_RSS_variant(fdgs, n)
		}
		// console.debug({res_nb_str})
		let res_nb = _.do_int(res_nb_str)
		if (isNaN(res_nb) || 0 === res_nb) {  // some sources don't have res_nb at all
			res_nb = fdgs_nb
		}
		let f_h1='', f_url='', f_by='', f_dt=new Date(0), f_nb=0, f_img=''
		for (let f of fdgs) {
			if (max_fdgs-- === 0) break
			f_nb = MAX_RES_BY_SRC - max_fdgs
			f_h1 = ''; f_url=''; f_by=''; f_img=''; f_dt=new Date(0)
			let l_fmp ={'f_img_src':'','f_img_alt':'','f_img_title':'','mp_data_img':'',
				'mp_data_alt':'','mp_data_title':'','f_txt':''}
			try {
				f_h1 = $_('r_h1', f, n, f_nb)
				if (!f_h1) throw 'missing this f_h1'
				f_dt = $_('r_dt', f, n, f_nb, false)
				if (!f_dt) throw `No f_dt after lookup (mp.$_) f_h1 ${f_h1} %%%`
				f_dt = parse_dt_str(f_dt, n.tags.tz, f_nb, n)
				if(isNaN(f_dt)) throw 'No f_dt after parse_dt_str'
				f_url = $_('r_url', f, n, f_nb)
				if (!f_url) throw 'no f_url after lookup'
				f_url = _.urlify(f_url, n.domain_part)
				if (!f_url) throw 'missing this f_url'
			} catch (exc) {
				r.errors.push(sµ.create_error(n, sµ.error_code.ERROR,
					f_nb + ' ' + exc, sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
				res_nb--
				fdgs_nb--
				continue
			}
			try {
				l_fmp.f_txt = $_('r_txt', f, n, f_nb)
				l_fmp = img_lookup(l_fmp, f_img, f, n, i, f_nb)
				f_by = $_('r_by', f, n, f_nb) || n.tags.name
				if (n.filter_results) {
					let f_full_txt = `${l_fmp.f_txt} ${f_h1}`.toLowerCase()
					// let f_full_txt = _.str_uniq_line(`${l_fmp.f_txt} ${f_h1}`.toLowerCase())
					let terms = search_terms.toLowerCase().split(' ')
					let terms_re = terms.map((a) => new RegExp(`${word_sep_re}${a}${word_sep_re}`, 'g'))
					let keep_result = terms_re.every(a => a.test(f_full_txt))
					if (!keep_result) {
						console.log('Discarded result', f_nb, 'from', n.tags.name,
							'because missing search terms', {l_fmp})
						//console.log('Discarded result', f_nb, 'from', n.tags.name, '"',l_fmp.f_txt, '"')
						res_nb--
						fdgs_nb--
						continue
					}
				}
				let f_text = TEST_SRC ? _.shorten(l_fmp.f_txt, EXCERPT_SIZE) :
					_.bolden(l_fmp.f_txt, search_terms)
				r.findings.push({f_h1:f_h1, f_url:f_url, f_by:f_by,
					f_txt: f_text,
					f_h1_title:	f_h1,
					f_by_title:	f_by,
					f_dt: f_dt.toISOString().slice(0, -14),
					f_epoc:	f_dt.valueOf(),
					f_ISO_dt: f_dt.toISOString(),
					f_dt_title: f_dt.toLocaleString(userLang),
					f_icon:	n.favicon_url,
					f_img_src: l_fmp.f_img_src,
					f_img_alt: l_fmp.f_img_alt,
					f_img_title: l_fmp.f_img_title,
					mp_data_img: l_fmp.mp_data_img,
					mp_data_alt: l_fmp.mp_data_alt,
					mp_data_title: l_fmp.mp_data_title,
					f_source: n.tags.name,
					f_source_title: n.tags.name,
					mp_data_key: i
				})
			} catch (exc) {
				r.errors.push(sµ.create_error(n, sµ.error_code.ERROR,
					f_nb + ' ' + exc, sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
			}
		}
		let local_res_nb = fdgs_nb === 0 ? fdgs_nb : res_nb
		r.meta_findings.push({
			mf_icon: n.favicon_url,
			mf_name: n.tags.name,
			mf_ext_link: format_search_url(n.search_url_web || n.search_url,
				search_terms, MAX_RES_BY_SRC),
			// mf_remove_source: n.tags.name,
			mf_res_nb: local_res_nb,
			mf_locale_res_nb: local_res_nb.toLocaleString(userLang),
		})
		console.log(i, 'duration', new Date() - src_query_start_date, 'ms', {res_nb})
	} catch (exc) {
		let err = exc.name == 'AbortError' ? sµ.error_code.NETWORK_ERROR : sµ.error_code.ERROR
		r.errors.push(sµ.create_error(n, err, exc,
			sµ.error_status.ERROR, 'test_src', {'article': i}, 'log'))
	}
	return r
}
function img_lookup(l_fmp, f_img, f, n, i, f_nb) {
	if (! n.type) {
		let is_r_img = _.is_str(n.r_img)
		if (is_r_img || _.is_str(n.r_img_xpath))
			if (is_r_img)
				f_img = f.querySelector(n.r_img)
				/* try { f_img = f.querySelector(n.r_img) }
				catch (exc) { error.push(sµ.create_error(n, sµ.error_code.ERROR,
					exc, sµ.error_status.ERROR, 'test_src', {'article': i})) }*/
			else // (_.is_str(n.r_img_xpath))
				f_img = evaluateXPath(f, n.r_img_xpath)
				/*try { f_img = evaluateXPath(f, n.r_img_xpath) }
				catch (exc) { error.push(sµ.create_error(n, sµ.error_code.ERROR,
					exc, sµ.error_status.ERROR, 'test_src', {'article': i})) }*/
	}
	if (_.is(f_img)) {
		if (f_img === '' && n.type === 'XML') { // extract image from f_txt
			let f_txt = l_fmp.f_txt
			let f_txt_dom = DOM_parser.parseFromString(
				HTML_decode_entities(f_txt), 'text/html')
			// f_img = f_txt_dom.querySelector('img')
			f_img = f_txt_dom.getElementsByTagName('img')[0] // it's not faster with Firefox 90.a1
			// it's slower in Chromium Version 90.0.4430.85 (Build officiel) Arch Linux (64 bits)
			// a benchmark shows it 2x faster in Firefox 96
			l_fmp.f_txt = f_txt_dom.body.textContent // removes image from f_txt
		}
		if (f_img) {
			l_fmp.f_img_src = _.urlify(f_img.attributes.src.value, n.domain_part)
			l_fmp.f_img_alt = strip_HTML_tags(f_img.alt) || ''
			l_fmp.f_img_title = strip_HTML_tags(f_img.title) || l_fmp.f_img_alt || ''
		}
	}
	if (_.is(n.r_img_src) || _.is(n.r_img_src_xpath)) {
		l_fmp.f_img_src = $_('r_img_src', f, n, f_nb)
		if (_.is(l_fmp.f_img_src))
			l_fmp.f_img_src = _.urlify(l_fmp.f_img_src, n.domain_part)
	}
	if (_.is(n.r_img_alt))
		l_fmp.f_img_alt = $_('r_img_alt', f, n, f_nb)
	if (_.is(n.r_img_title))
		l_fmp.f_img_title = $_('r_img_title', f, n, f_nb)
	if (!l_fmp.f_img_title)
		l_fmp.f_img_title = l_fmp.f_img_alt
	l_fmp.mp_data_img = l_fmp.f_img_src
	l_fmp.mp_data_alt = l_fmp.f_img_alt
	l_fmp.mp_data_title = l_fmp.f_img_title
	// if (!IS_LOAD_IMG && l_fmp.f_img_src)  // garbadge from the past
	// 	img_not_load(l_fmp)
	return l_fmp
}
/**
 *
 */
export function DOM_parse_text_rep (txt_rep, c_type) {
	c_type = c_type.split(';')[0].replace(/(rss|atom)\+/, '')
	// console.log('txt_rep', txt_rep)
	// LMD.no XML Parsing Error: undefined entity
	// Revue Ballast
	// Tel Quel (ma) XML Parsing Error: undefined entity ; InternalError: too much recursion
	txt_rep = txt_rep.trim()
	txt_rep = _.drop_low_unprintable_utf8(txt_rep)
	let rep = DOM_parser.parseFromString(txt_rep, c_type)
	if (is_DOM_parsererror(rep, false)) {  // true would console.error the error
		if (c_type.includes('xml')) {  // what was the source needing this ?
			// Sources needing decode / encode : LDM.no, Revue Ballast, Tel Quel (ma)
			txt_rep = HTML_decode_entities(txt_rep)
			// here '&' aren't encoded in URL anymore
			txt_rep = _	.XML_encode_UTF8(txt_rep)
			txt_rep = drop_itunes_namespace(txt_rep)
			rep = DOM_parser.parseFromString(txt_rep, c_type)
		}
		if (is_DOM_parsererror(rep, true))
			// return null
			throw 'DOM parser error'
	}
	// console.log(rep)
	return rep
}
/**
 *
 */
function drop_itunes_namespace(txt) { // 2023: Science.org RSS miss this namespace declaration
	return txt.replace(/<itunes:.*?(\/>|<\/itunes:.{2,9}?>)/ig, '')
}
/**
 * Parsing lookup
 */
export function $_(elt, dom, src, f_nb=0, errors=[], do_txt=true, silent=false) {
	// console.log('$_(elt, dom, src, f_nb=0, do_txt=true, silent=false)', elt, dom, src, f_nb)
	let local_dbg=dbg
	// if (elt == 'r_img_src') local_dbg=true
	let val
	let src_elt = src[elt]
	if (src.type === 'JSON') {
		if (!_.is(src_elt)) return '' // we need undef src_elt to seek _xpath
		if (Array.isArray(src_elt)) {
			try {
				let val_lst = []
				for (let val_idx of src_elt)
					val_lst.push(strip_HTML_tags(_.deref_JSON_path(val_idx, dom) || '') || '')
				val = _.str_fmt(src[`${elt}_tpl`], val_lst)
			} catch (exc) {
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.ERROR, 'test_src', {}, 'log'))
				// add_err(src, 0, `${f_nb}: ${elt} ${exc}`, 'warn', 'test_src')
				// console.debug(src.tags.name, {src}, `${f_nb}: ${elt}`, exc, {exc})
			}
		} else {
			try {
				val = _.deref_JSON_path(src_elt, dom) || ''
				if (elt !== 'results' && Array.isArray(val)) {  // if wanted elt 'results' return them
					let val_acc = ''
					let val_attr = src[`${elt}_attr`]
					for (let v of val) {
						if (_.is_str(val_attr)) {
							val_acc += `${v[val_attr]}, `
						} else {
							val_acc += v
						}
					}
					val = val_acc
				}
			} catch (exc) {
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.ERROR, 'test_src', {}, 'log'))
			}
		}
	} else {
		if (_.is_str(src_elt)) {
			val = dom.querySelector(src_elt)
			local_dbg && console.debug('val querySelector', val, 'for src_elt', src_elt)
		} else if (Array.isArray(src_elt)) {
			try {
				return lookup_tpl(dom, src, elt, src_elt, null, do_txt)
			} catch (exc) {
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.ERROR, 'test_src', {}, 'log'))
			}
		} else {
			let elt_xpath = src[`${elt}_xpath`]
			if (_.is_str(elt_xpath)) {
				val = evaluateXPath(dom, elt_xpath)
			} else if (Array.isArray(elt_xpath)) {
				try {
					return lookup_tpl(dom, src, elt, elt_xpath, 'xpath', do_txt)
				} catch (exc) {
					!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
						`${f_nb}: ${elt} ${exc}`, sµ.error_status.ERROR, 'test_src', {}, 'log'))
				}
			} else {
				return '' // `Missing ${elt} (or ${elt}_xpath) in ${src.tags.name}, or not string.`)
			}
		}
		let src_elt_attr = src[`${elt}_attr`]
		if (_.is_str(src_elt_attr)) {
			let val_attr
			try {
				val_attr = val.attributes[src_elt_attr]
				val = strip_HTML_tags(val_attr && val_attr.value || val[src_elt_attr] || '')
			} catch (exc) {
				val = ''
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.ERROR, 'test_src', {}, 'log'))
			}
			// } else if (! Array.isArray(src_elt)) { // is it possible to be an array here ?
			// } else if (typeof(src_elt) !== 'object') { // this prevents XPath objects to be treated
		} else {
			try {
				val = _.triw(val.textContent)
			} catch (exc) {
				val = (elt === 'r_dt') ? '100000000000' : ''  // date : 1973-03-03 09:46:40 GMT+0000
				!silent && errors.push(sµ.create_error(src, sµ.error_code.ERROR,
					`${f_nb}: ${elt} ${exc}`, sµ.error_status.ERROR, 'test_src', {}, 'log'))
			}
		}
	}
	// local_dbg && console.debug(elt, 'value before _re', val)
	// if (elt == 'r_img_src') console.log('before', val)
	let elt_re = src[`${elt}_re`]  // it should be a list of 2 elements
	if (_.is_obj(elt_re)) {
		if (_.is_str(val)) {
			val = _.regextract(elt_re[0], val, elt_re[1])
		} else {  // let's hope it's a Number
			let val_type = typeof (val)
			val = String(val)
			val = _.regextract(elt_re[0], val, elt_re[1])
			if (val_type === 'number')
				val = Number(val)
		}
	}
	// local_dbg && console.debug(elt, 'value before strip_HTML_tags', val)
	if (val && do_txt)
		val = strip_HTML_tags(val)
	local_dbg && console.debug(elt, 'returned val', val)
	return val
}
/**
 *
 */
function lookup_tpl (dom, src, elt, src_elt, is_xpath=false, do_txt=true) {
	let val_lst = []
	let val_attr = src[`${elt}_attr`]
	let z
	for (let val_idx of src_elt) {
		if (is_xpath) {
			val_lst.push(evaluateXPath(dom, val_idx) || '')
		} else {
			val_lst.push(dom.querySelector(val_idx) || '')
		}
		z = val_lst.length - 1
		if (Array.isArray(val_attr) && val_attr[z]) {
			let tpl_elt_attrs = val_lst[z].attributes[val_attr[z]]
			val_lst[z] = strip_HTML_tags(tpl_elt_attrs && tpl_elt_attrs.value || '')
		} else {
			val_lst[z] = _.triw(val_lst[z].textContent)
		}
	}
	dbg && console.debug('val_lst', val_lst)
	let val = _.str_fmt(src[`${elt}_tpl`], val_lst)
	if (val && do_txt) val = strip_HTML_tags(val)
	dbg && console.debug(`${elt}_tpl returned:`, val)
	return val
}
export function find_RSS_variant(items, n) { /* eslint-disable dot-notation */
	let j = 0
	let src_def = {}
	const light_n = {tags: n.tags}
	const silent = true
	const do_txt = true
	for (let i of items) {
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_image:image'], light_n)
		if ($_('r_img_title',i, src_def, j, do_txt, silent)) return 'RSS_image:image'
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_enclosure'], light_n)
		if ($_('r_img_src',  i, src_def, j, do_txt, silent)) return 'RSS_enclosure'
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_media:thumbnail'], light_n)
		if ($_('r_img_src',  i, src_def, j, do_txt, silent)) return 'RSS_media:thumbnail'
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_media:content'], light_n)
		if ($_('r_img_src',  i, src_def, j, do_txt, silent)) return 'RSS_media:content'
		// 2023-07-13 : added r_img_src_re in image-less XML source definitions
		//if (i.innerHTML.includes('img') && i.innerHTML.includes('src'))
		//  console.info('XML from', n.tags.name, 'might contain undetected img', i)
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_dc:date'], light_n)
		if ($_('r_dt',       i, src_def, j, do_txt, silent)) return 'RSS_dc:date'
		src_def = sµ.extend_source(sµ.XML_SRC['RSS_content:encoded'], light_n)
		if ($_('r_txt',      i, src_def, j, do_txt, silent)) return 'RSS_content:encoded'
		src_def = sµ.extend_source(sµ.XML_SRC['ATOM_content'], light_n)
		if ($_('r_txt',      i, src_def, j, do_txt, silent)) return 'ATOM_content'
		src_def = sµ.extend_source(sµ.XML_SRC['ATOM'], light_n)
		if ($_('r_txt',      i, src_def, j, do_txt, silent)) return 'ATOM'
		j += 1
	} /* eslint-enable dot-notation */
	return 'RSS'
}
export function set_RSS_variant(items, n) {
	n.xml_type = find_RSS_variant(items, n)
	// console.debug('RSS_varian', n.xml_type)
	return sµ.extend_source(sµ.XML_SRC[n.xml_type], n)
}
/*
 *
 */
function get_src_dt_locale(n) {
	let date_locale = n.tags.date_locale
	if (date_locale) {
		if (date_locale === 'browser')
			return get_browser_locale()
		return date_locale
	}
	return `${n.tags.lang}-${n.tags.country}`
}
/**
 * Parses a date string from a website in a Date object with the given regex and format.
 * The format is in general `years-months-days hours:minutes`.
 * If months is not a number (but a word) use {} to translate this month name in its number:
 * $years-{$months}-$days $hours:minutes.
 * Another posible format is : "$number (sec|min|hour|today|day|yesterday|week|month|year)"
 * for the date like: 5 minutes ago.
 * @param {string} dt_str The date to parse
 * @param {string} tz The timezone of the date
 * @param {Array} f_nb The format "$4-{$3}-$2 $1"
 * @param {Array} n The regex to extract the data
 * @returns {Date}
 */
export function parse_dt_str(dt_str, tz, f_nb, n) {
	let d = undefined
	let i = 1
	let cur_fmt
	let dt_repl = ''
	dbg && console.debug('dt before', {dt_str})
	if (cur_fmt = n[`r_dt_fmt_${i}`]) {
		do {
			if(typeof(dt_str) === 'number')
				dt_str = String(dt_str)
			dt_repl = _.regextract(cur_fmt[0], dt_str, cur_fmt[1])
			dbg && console.debug(`dt tried fmt_${i} [${cur_fmt}] got`, {dt_repl})
			if (dt_repl !== '')
				d = sub_parse_dt_str(dt_repl, tz, f_nb, get_src_dt_locale(n))
			i += 1
		} while ((cur_fmt = n[`r_dt_fmt_${i}`]) && isNaN(d))
	} else {
		d = sub_parse_dt_str(dt_str, tz, f_nb, get_src_dt_locale(n))
	}
	if (isNaN(d)) throw new Error(`'${dt_str}' (${tz}) is an invalid new Date()`)
	dbg && console.debug('dt after', dt_str, {d})
	return d
}
/**
 * With a formated date, tries to create a Date object.
 * @param {string} dt_str the date formated
 * @param {string} tz the timezone
 * @param {string} f_nb the format
 * @returns {Date} The foud date
 */
function sub_parse_dt_str(dt_str, tz, f_nb, lang) {
	let d = NaN
	dbg && console.debug('sub_parse_dt_str(dt_str, tz, f_nb, lang)', dt_str, tz, f_nb, lang)
	if (_.is_str(dt_str))  // JSON timestamps are integers
		dt_str = dt_str.replace(/{(.*)}/, (_, $1) => lang_month_nb(lang, $1))
	if (MM_DD_ONLY_RE.test(dt_str)) {
		let today = timezoned_date('', tz)
		const cur_month = today.getMonth()  // starts at 0 == january
		if (cur_month < Number(dt_str.replace(MM_DD_ONLY_RE.test, '$1')))
			dt_str = `${today.getFullYear()-1}${dt_str}`
		else
			dt_str = `${today.getFullYear()}${dt_str}`
	} else if (HH_MM_ONLY_RE.test(dt_str)) {
		let today = timezoned_date('', tz)
		dt_str = `${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()} ${dt_str}`
	}
	dbg && console.info('sub_parse_dt_str dt_str', dt_str)
	d = timezoned_date(dt_str, tz)
	dbg && console.info('d = timezoned_date(dt_str, tz)', d)
	if(isNaN(d) && /\d{12,20}/i.test(dt_str)) {
		dbg && console.debug('found it\'s a timestamp')
		d = new Date(Number(dt_str))
	}
	if(isNaN(d) && ['sec','min','hour','today','day','yesterday','week','month','year'].some(
		a => dt_str.includes(a))
	) {
		d = timezoned_date('', tz)
		const regex_nb = /[^\d]*(\d+)[^\d]*/
		if (/sec/i.test(dt_str)) {
			d.setSeconds(d.getMinutes() - Number(dt_str.replace(/sec?.*/, '')))
		} else if (/min/i.test(dt_str)) {
			d.setMinutes(d.getMinutes() - Number(dt_str.replace(/min?.*/, '')))
		} else if (/hour/i.test(dt_str)) {
			d.setHours(d.getHours() - Number(dt_str.replace(/hour.*/, '')))
		} else if (/yesterday/i.test(dt_str)) {
			d.setDate(d.getDate() - 1)
			d.setMinutes(0)
			d.setSeconds(0)
		} else if (/today/i.test(dt_str)) {
			d.setMinutes(0)
			d.setSeconds(0)
		} else if (/day/i.test(dt_str) ) {
			d.setDate(d.getDate() - Number(dt_str.replace(regex_nb, '$1')))
		} else if (/week/i.test(dt_str) ) {
			d.setDate(d.getDate() - (Number(dt_str.replace(regex_nb, '$1')) * 7))
		} else if (/month/i.test(dt_str) ) {
			d.setMonth(d.getMonth() - Number(dt_str.replace(regex_nb, '$1')))
		} else if (/year/i.test(dt_str) ) {
			d.setFullYear(d.getFullYear() - Number(dt_str.replace(regex_nb, '$1')))
		}
	}
	if (d && !d.getMinutes() && !d.getSeconds()) {	// no time set ? put an ordered one
		if (HH_MM_RE.test(dt_str)) {
			let d_split = _.regextract(HH_MM_STR, dt_str).split(':')
			d.setHours(Number(d_split[0]))
			d.setMinutes(Number(d_split[1]))
		} else {
			d.setHours(0)
			d.setMinutes(f_nb % 60)
		}
	}
	return d
}
/**
 * An arbitrary french internationalisation number format. What's important is to use the same
 * anywhere. Meta-Press.es has been created by a french guy.
 * @type {Intl.NumberFormat}
 */
/* const intlNum = Intl.NumberFormat('fr', {
	minimumIntegerDigits: 4,
	useGrouping: 0,
	signDisplay: 'always'})
*/
// const intl_datetime_format = new Intl.DateTimeFormat('fr',
//	{timeZoneName: 'short', timeZone: tz})
/**
 * Returns the given date transformed regarding to the navigator timezone.
 * If nav_to_tz is true the timezone offset is added, else it is removed
 * else returns the date from the given timezone
 * @param {string} dt_str The date
 * @param {string} tz The timezone
 * @param {boolean} nav_to_tz The direction of the conversion
 * @returns {Date} the date converted
 */
export function timezoned_date (dt_str, tz='UTC', nav_to_tz=false) {
	// let dt = new Date(dt_str)
	const dt = dt_str ? new Date(dt_str) : new Date()
	dbg && console.log('timezoned_date dt_str', dt_str, 'dt', dt, 'tz', tz)
	if (isNaN(dt)) return NaN
	// if (isNaN(dt))
	//	dt = new Date()
	if ('UTC' === tz || 'GMT' === tz) return dt
	const dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000)  // to get it in ms
	const dt_UTC	= new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000)
	dbg && console.log('dt', dt, '\ndt_orig', dt_orig, '\ndt_UTC', dt_UTC)
	let tz_offset
	if (typeof(tz) === 'number') {  // NaN is a number, '' is not NaN…
		const tz_hours = tz / 60
		const tz_minutes = Math.abs(tz % 60)
		tz_offset = `${String(tz_hours)}:${String(tz_minutes)}`
	} else {  // usually the tz is a string like : Europe/Paris
		const dt_repr = dt_UTC.toLocaleTimeString('fr', {timeZoneName: 'short', timeZone: tz})
		// dt_UTC.toLocaleTimeString -> should be replaced by Intl.DateTimeFormat
		// BUT tz changes at each call
		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/DateTimeFormat/format
		dbg && console.log('dt_repr', dt_repr)
		tz_offset = dt_repr.split('UTC')[1]
		if ('' === tz_offset)  // so it's UTC finally
			return dt
		if (!_.is_in(tz_offset, ':'))
			tz_offset = `${tz_offset}:00`
		if (tz_offset.length < 6)
			tz_offset = `${tz_offset[0]}0${tz_offset.slice(1)}`
		if ('+' !== tz_offset[0])
			tz_offset = `-${tz_offset.slice(1)}`
	}
	dbg && console.log('tz_offset', tz_offset, 'typeof tz', typeof(tz))
	let date_tz
	if (nav_to_tz) {
		date_tz = dt_UTC
		const tz_sign = tz_offset[0]
		tz_offset = `${'+' === tz_sign ? '-' : '+'}${tz_offset.slice(1)}`
	} else {
		date_tz = dt_orig
	}
	dbg && console.log('date_tz.toISOString', date_tz.toISOString(), 'tz_offset', tz_offset)
	// example -> Date.parse("2019-01-01T00:00:00.000+00:00");
	const new_tzd_date = date_tz.toISOString().replace(/Z/, tz_offset)
	dbg && console.log('new_tzd_date', new_tzd_date)
	return new Date(new_tzd_date)
}
/**
 * Extract an HTML tag attribute attr from a string str supposed to contain HTML.
 * @param {string} attr Attribute to extract
 * @param {string} str The string to search in
 * @returns {string} The value of the 1st attribute
 */
/*export function extract_val_attr (attr, str) {
	let s = regextract(` ${attr}=["'](.*?)["']`, str)
	return (s === str ? '' : s)
}*/
/**
 * The string to build a regular expression to extract hour:minutes from a date.
 * @type {string}
 */
export const HH_MM_STR = '(\\d\\d?:\\d\\d)'
/**
 * The regular expression allowing to extract hour:minutes from a date.
 * @type {RegExp}
 */
export const HH_MM_RE = new RegExp(` ${HH_MM_STR}`, 'i')
/**
 * The regex to extract a string with just hour:minutes.
 * @type {RegExp}
 */
export const HH_MM_ONLY_RE = new RegExp(`^${HH_MM_STR}$`, 'i')
/**
 * The string to build a regular expression to extract -month-day in a date.
 * @type {string}
 */
export const MM_DD_ONLY_STR = '^-(\\d{1,2})-(\\d{1,2})'
/**
 * The regular expression to extract -month-day in a date.
 * @type {RegExp}
 */
export const MM_DD_ONLY_RE = new RegExp(MM_DD_ONLY_STR, 'i')
