// SPDX-FileName: ./source_utils.js
// SPDX-FileCopyrightText: 2022-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

import * as _ from './js_utils.js'

/**
 * @module source_utils
 */
/**
 * Source definitions loading / building / reloading…
 * @namespace source_loading
 */
/**
 * Source definition for various XML-based formats (lik RSS or ATOM).
 * @memberof source_loading
 * @type {Object}
 */
export const XML_SRC = {
	'RSS': {	// filters: where we store the source filters in exported XML files
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		// 'r_img': 'NOT MANAGED',
		'r_by_xpath': './dc:creator',
		'docs': 'https://www.w3.org/TR/selectors-3/#attrnmsp',
		'r_img_src': 'description',
		'r_img_src_re': [
			'src="(.+?)"',
			'$1'
		],
		'r_img_alt': 'description',
		'r_img_alt_re': [
			'alt="(.+?)"',
			'$1'
		],
		'r_img_title': 'description',
		'r_img_title_re': [
			'title="(.+?)"',
			'$1'
		],
	},
	'RSS_content:encoded': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt_xpath': './content:encoded',
		'r_by_xpath': './dc:creator',
		'r_img_src': 'description',
		'r_img_src_re': [
			'src="(.+?)"',
			'$1'
		],
		'r_img_alt': 'description',
		'r_img_alt_re': [
			'alt="(.+?)"',
			'$1'
		],
		'r_img_title': 'description',
		'r_img_title_re': [
			'title="(.+?)"',
			'$1'
		],
	},
	'RSS_dc:date': {	// basta.media
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt_xpath': './dc:date',
		'r_txt': 'description',
		'r_img_src': 'description',
		'r_img_src_re': [
			'src="(.+?)"',
			'$1'
		],
		'r_img_alt': 'description',
		'r_img_alt_re': [
			'alt="(.+?)"',
			'$1'
		],
		'r_img_title': 'description',
		'r_img_title_re': [
			'title="(.+?)"',
			'$1'
		],
	},
	'RSS_enclosure': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		'r_by_xpath': './dc:creator',
		'r_img_src': 'enclosure',
		'r_img_src_attr': 'url'
	},
	'RSS_media:content': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		'r_by_xpath': './dc:creator',
		'r_img_src_xpath': './media:content/@url',
		'r_img_title_xpath': './edia:content/media:title'
	},
	'RSS_image:image': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		'r_by_xpath': './dc:creator',
		'r_img_src_xpath': './image:image/image:loc',
		'r_img_title_xpath': './image:image/image:title',
		'r_img_alt_xpath': './image:image/image:caption'
	},
	'RSS_media:thumbnail': {
		'filters': 'category',
		'results': 'item',
		'r_h1': 'title',
		'r_url': 'link',
		'r_dt': 'pubDate',
		'r_txt': 'description',
		'r_by_xpath': './dc:creator',
		'r_img_src_xpath': './media:thumbnail',
		'r_img_src_attr': 'url'
	},
	'ATOM': {
		'filters': 'category',
		'results': 'entry',
		'r_h1': 'title',
		'r_url': 'link',
		'r_url_attr': 'href',
		'r_dt': 'published',
		'r_txt': 'summary',
		'r_by': 'author'
	},
	'ATOM_content': {
		'filters': 'category',
		'results': 'entry',
		'r_h1': 'title',
		'r_url': 'link',
		'r_url_attr': 'href',
		'r_dt': 'published',
		'r_txt': 'content',
		'r_by': 'author'
	}
}
/**
 * Returns the JSON object defining the provided sources before any modifications
 * @memberof source_loading
 * @returns {Object} The raw JSON object of provided sources
 */
export async function get_raw_src() {
	let src = await fetch('../json/sources.json')
	return await src.json()
}
/**
 * Extends a source from another given source.
 * @memberof source_loading
 * @param {Object} n The source that inherits its definition from another source
 * @param {Object} extended_src The mother source
 * @returns {Object} The extended source n
 */
export function extend_source(n, extended_src) {
	let new_src = Object.assign({}, extended_src, n)
	new_src.tags = Object.assign({}, extended_src.tags, n.tags)
	return new_src
}

export async function import_custom_src(built_src, custom_src) {
	try {
		for (const i of Object.keys(custom_src)) {
			if (built_src[i]) custom_src[i].overwrite = true
			custom_src[i].custom = true
		}
		built_src = Object.assign(built_src, custom_src)
	} catch (exc) { alert(`Error parsing user custom source (${exc}).`) }
	return built_src
}
/**
 * Loads and build the sources object from the JSON definitions (provided and locally defined)
 * The building process actually extends the sources marked as so or XML types, removes the
 * broken-tagged ones, and adds the computed tags (such as HTTP/HTTPS, Illustrated…).
 * @memberof source_loading
 * @returns {Object} The sources object
 */
export async function build_src(custom_src) {
	let built_src = await get_raw_src()
	if (custom_src)
		import_custom_src(built_src, custom_src)
	// for (let [k, n] of Object.entries(built_src)) {	// we need to remove entries while looping
	for (let k of Object.keys(built_src)) {
		let n = built_src[k]
		let n_tech = n.tags.tech
		if (n_tech && n_tech.includes('broken')) {
			delete built_src[k]
			continue
		}
		// if (!k.startsWith('http')) continue	// was used to avoid false RSS and ATOM
		if (_.is(n.extends)) { // Manage sources to extend
			let extended_src = built_src[n.extends]
			if (_.is(extended_src))
				n = extend_source(n, extended_src)
			else {
				console.error(`${k} extends a broken or missing source ${n.extends}.`)
				delete built_src[k]
				continue
			}
		}
		n.tags.key = k
		n_tech = n.tags.tech
		if (!n.domain_part || n.extends) n.domain_part = _.domain_part(k)
		// if (n.search_url.startsWith('https')) // Dynamically add the HTTPS src tag
		//	_.array_add_once(n_tech, 'HTTPS')
		if (n.type && n.type == 'XML') // Dynamically add the XML src tag
			_.array_add_once(n_tech, 'XML')
		let src_type = n.tags.src_type
		if (n.r_img || n.r_img_src) // Dynamically add the Illustrated src tag
			_.array_add_once(src_type, 'Illustrated')
		if (['Press', 'Magazine', 'Institution', 'Radio', 'TV', 'Doc.'].some(
			a => _.array_contains(src_type, a))
		)
			_.array_add_once(src_type, 'Secondary source')
		built_src[k] = n
	}
	return built_src
}
/*
 *
 */
export function get_all_search_urls(current_source_selection, source_objs) {
	let all_search_urls = []
	for (let i of current_source_selection) {
		let n = source_objs[i]
		if (!_.is(n)) return console.error('Unknown source', i)
		if (n.search_url)
			all_search_urls.push(n.search_url)
		if (n.redir_url)
			if (Array.isArray(n.redir_url))
				all_search_urls.concat(n.redir_url)
			else
				all_search_urls.push(n.redir_url)
	}
	return all_search_urls
}
/**
 * Creates and returns a copy of the source objects or just the src_name part of sources
 * @memberof source_loading
 * @param {string} src_names The name of the sources to return
 * @returns {Object} The sources object
 */
export function get_prov_src(src_names, built_src) {
	if(src_names === '') return built_src
	let prv = {}
	// for (let t of Object.keys(built_src)) {
	//	prv[t] = built_src[t]
	// }
	if(_.is_str(src_names)) {
		src_names = [src_names]
	}
	if(Array.isArray(src_names)) {
		for(let src_name of src_names) {
			for (let t of Object.keys(built_src)) {
				if(built_src[t].tags.name === src_name) {
					prv[t] = built_src[t]
					//let ext = prv[t].extends
					//if(ext) {prv[ext] = provided_sources[ext]}
					break
				}
			}
		}
	}
	return prv
}
/**
 * @constant The enumeration of the error types.
 */
export const error_code = {
	ERROR: '0',						// not recognized error
	INVALID_URL: 'url_0',			// regex url
	NETWORK_ERROR: 'url_1',			// network
	NO_RESULT: 'result_0',			// no result
	BAD_VALUE: 'value_0',			// malformed value
	INVALID_VALUE: 'value_1',			// invalid, no referenced value
	FATHER_NOT_FOUND: 'value_2',	// father (extends) not found
	DEPRECATED_ATTR: 'attr_0',		// attribute deprecated
	NOT_FOUND_ATTR: 'attr_1',		// attribute not found
	CONFLICTS_ATTR: 'attr_2',		// conflict with attributes
	NAME_ALREADY_TAKEN: 'attr_3', // src name is taken
	SUPERFLUOUS_ATTR: 'attr_4',  // for unknown attributes
	RSS_POSSIBLE: 'rss_0',			// If rss is possibly found
}
/**
 * @constant The enumeration of the error status.
 */
export const error_status = {
	ERROR: 'error',
	WARNING: 'warn',
	INFO: 'info',
	LOG: 'log'
}
/**
 * Creates an error object fitted for the add_error function of the source testing framework
 * @param {string} error_code The code of the error
 * @param {string} src_name The name of the source
 * @param {string} text Error description
 * @param {string} level the error level : warn, error
 * @param {Object} other The other attribute data to add to object
 */
export function create_error(src_def, error_code, text, level, dest, other={}, log) {
	let src_key = src_def.tags.key
	if (log) {
		console[level](src_key, text)
		if (level == 'warn')
			console.trace()
	}
	let trace =  new Error()
	let n_error = {
		'src_def': src_def,
		'code': error_code,
		'src_key': src_key,
		'text': text,
		'level': level,
		'dest': dest,
		'stack_trace': trace.stack
	}
	// console[level](src_name, text)
	Object.assign(n_error, other)
	return n_error
}

export const SRC_FORMAT_ORDER = [
	'extends',
	'favicon_url',
	'news_rss_url',
	'token',
	'token.token_url',
	'token.token_method',
	'token.token_ctype',
	'token.token_body',
	'token.token_type',
	'token.token_sel',
	'type',
	'search_url',
	'search_url_web',
	'method',
	'search_ctype',
	'body',
	'redir_url',
	'domain_part',
	'json_to_html',
	'raw_rep',	// -> raw_rep_re (was jsonp_to_json_re)
	'res_nb',
	'results',
	'r_h1',
	'r_url',
	'r_dt',
	'r_dt_fmt_',
	'r_txt',
	'r_img',
	'r_img_src',
	'r_img_alt',
	'r_img_title',
	'r_by',
	'filter_results',
	'tags',
	'tags.name',
	'tags.lang',
	'tags.country',
	'tags.date_locale',
	'tags.themes',
	'tags.tech',
	'tags.src_type',
	'tags.res_type',
	'tags.tz',
	'tags.charset',
	'tags.notes',
	'tags.what_to_fix',
	'tags.warn_mask',
	'tags.key',
	'positive_test_search_term'
]
/**
 * Formats the sources.json in the given order. Furthermore r_dt_fmt describes all
 * the r_dt_fmt and all attributes except entries with _xpath, _attr, _re, _tpl, _com
 * dynamically added. The missing attributes are pushed at the end of the corresponding source.
 * @param {Object} raw_src - The sources.json unbuild Object
 * @param {Array} order - The order of the element in the sources
 * @returns {Object} - The well formated raw sources
 */
export function reformat_sources(raw_src, order) {
	let new_src = {}
	for (const i of Object.keys(raw_src))  // remove trailing slashes
		new_src[_.rm_trailing_slash(i)] = reformat_source(raw_src[i], order)
	return new_src
}
export function reformat_source(raw_i, order) {  // new_i is a source definition
	let new_i = {}
	for (const attr of order) {
		if (attr === 'tags') {
			new_i[attr] = {}
		} else if (attr.startsWith('tags.')) {
			let tag_name = attr.replace('tags.', '')
			new_i.tags[tag_name] = raw_i.tags[tag_name]
		} else if (attr === 'r_dt_fmt_') {
			for (const j of Object.keys(raw_i))
				if (j.startsWith('r_dt_fmt_'))
					new_i[j] = raw_i[j]
		} else {
			for (let suff of ['', '_xpath', '_attr', '_re', '_tpl', '_com']) {
				let attr_stuff = `${attr}${suff}`
				if (raw_i[attr_stuff]) {
					new_i[attr_stuff] = raw_i[attr_stuff]
					if (_.is_str(raw_i[attr_stuff]))
						if (attr.endsWith('_url') || attr.endsWith('_url_web') || attr === 'extends')
							if (raw_i.tags.name !== 'Haaretz')
								new_i[attr_stuff] = _.rm_trailing_slash(new_i[attr_stuff])
				}
			}
		}
	}
	for (let j of Object.keys(raw_i))  // don't lose something unknown
		if (j !== 'tags')
			new_i[j] = raw_i[j]
	for (let j of Object.keys(raw_i.tags))
		try {
			if (!new_i.tags[j])
				new_i.tags[j] = raw_i.tags[j]
		} catch (exc) {
			console.warn(raw_i.tags.name, `new_src.tags[${j}] not found`)
		}
	return new_i
}
/*
 *
 */
export function src_to_json(raw_src) {
	return JSON.stringify(reformat_sources(raw_src, SRC_FORMAT_ORDER), null, '\t')
}
/*
 *
 */
export function get_search_url(n) { return n.search_url_web || n.search_url }
