package com.example.metapress

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import com.example.metapress.ui.theme.MetaPressTheme
import org.mozilla.geckoview.GeckoRuntime
import org.mozilla.geckoview.GeckoSession
import org.mozilla.geckoview.GeckoView

class MainActivity : ComponentActivity() {
    private lateinit var geckoSession: GeckoSession
    private var isExtensionInstalled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Create a GeckoRuntime and a GeckoSession
        val geckoRuntime = GeckoRuntime.create(this)
        geckoSession = GeckoSession()
        geckoSession.open(geckoRuntime)

        // Load a blank page initially
        geckoSession.loadUri("about:blank")

        // Install the built-in extension
        geckoRuntime.webExtensionController
            .installBuiltIn("resource://android/assets/messaging/meta-press-ext/")
            .accept({
                Log.d("WebExtension", "Extension installed successfully")
                isExtensionInstalled = true // Mettre à jour l'état de l'installation
            }, { error ->
                Log.e("WebExtension", "Error installing extension", error)
            })

        setContent {
            MetaPressTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    if (isExtensionInstalled) {
                        GeckoWebView(geckoSession)
                    } else {
                        InstallExtensionMessage()                    }
                }
            }
        }
    }
}

@Composable
fun GeckoWebView(geckoSession: GeckoSession) {
    AndroidView(
        factory = { context ->
            GeckoView(context).apply {
                setSession(geckoSession)
            }
        },
        modifier = Modifier.fillMaxSize()
    )
}

@Composable
fun InstallExtensionMessage() {
    Text(
        text = "L'extension n'est pas installée. Veuillez installer l'extension pour continuer.",
        textAlign = TextAlign.Center,
        modifier = Modifier.fillMaxSize().padding(16.dp),
        //style = MaterialTheme.typography.body1
    )
}