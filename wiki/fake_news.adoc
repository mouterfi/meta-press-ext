= Fake news source

== BFMTV

- 2023-02-16 https://www.humanite.fr/medias/bfmtv/team-jorge-l-officine-israelienne-de-desinformation-qui-infiltre-bfmtv-782995[« Team Jorge », l’officine israélienne de désinformation qui infiltre BFMTV]
- 2023-02-15 https://www.francebleu.fr/infos/faits-divers-justice/enquete-derriere-un-soupcon-d-ingerence-etrangere-a-bfmtv-une-societe-de-desinformation-israelienne-1930980[ENQUÊTE - Derrière un soupçon d'ingérence étrangère à BFMTV, une société de désinformation israélienne]

== France Soir

- 2022-12-01 https://www.leparisien.fr/societe/epingle-pour-ses-fausses-informations-france-soir-nest-plus-reconnu-comme-un-service-de-presse-30-11-2022-L3Z3M64OYRBJLJFJDFHG4PHNAU.php[Épinglé pour ses fausses informations, France Soir n’est plus reconnu comme un service de presse]

== Le Point

- 2022-06-23 https://www.nouvelobs.com/politique/20220623.OBS60075/garrido-et-corbiere-employeurs-d-une-femme-de-menage-sans-papiers-le-point-retire-son-article.html[Garrido et Corbières employeurs d'une femme de ménage sans papiers, Le Point retire son article]
- 2022-07-10 https://www.mediapart.fr/journal/france/010722/garrido-corbiere-le-point-un-journal-accro-aux-fausses-infos[Garrido-Corbière : « Le Point », un journal accro aux fausses infos]

== Russia Today

- 2021-12-22 https://www.usinenouvelle.com/article/l-allemagne-interdit-la-diffusion-par-satellite-de-rt-deutsch.N1171237[L'Allemagne interdit la diffusion par satellite de RT Deutsch]
- 2022-03-18 https://www.lefigaro.fr/medias/le-royaume-uni-retire-sa-licence-de-diffusion-a-la-chaine-russe-rt-20220318[Le Royaume-Uni retire sa licence de diffusion à la chaîne russe RT]
- 2022-07-27 https://www.lest-eclair.fr/id394434/article/2022-07-27/rt-france-la-justice-europeenne-confirme-la-suspension-du-media-russe?referer=%2Farchives%2Frecherche%3Fdatefilter%3Danytime%26sort%3Ddate%2520desc%26word%3Dinterdiction%2520russia%2520today[RT France: la justice européenne confirme la suspension du média russe]
- 2023-04-12 https://www.la-croix.com/Economie/chaine-RT-France-fermer-site-francophone-restera-actif-2023-01-22-1201251783[La chaîne RT France va fermer mais son site francophone « restera actif »]
