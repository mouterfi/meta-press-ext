# Télécharger et installer le logiciel android studio 
- vous pouvez télécharger android studio via le lien : https://android-studio.fr.uptodown.com/android/telecharger 
- si vous avez une machine assez puissante vous pouvez télécharger la dernière version, sinon penser à revenir vers les versions un peu plus anciennes

# importer le projet
 
- Télécharger le dossier MyApplication2 
- Sur android studio vous pouvez importer un nouveau projet : File /new / import project 

- Vous choisissez le dossier MyApplication2

# Exécuter le projet
Apres avoir importé le dossier deux fichiers s'ouvrent : 
- un fichier qui contient le code kotlin (MainActivity.kt)
- un fichier xml qui contient le design de l'application (activity_main.xml)
- vous cliquez sur run pour la compilation et l'exécution ou Maj+F10

# GeckoView

geckoview est déjà injecté dans l'application,
j'ai ajouté ces lignes dans le fichier build.gradle : 

```kotlin
ext {
    
    geckoview_channel = "nightly"

    geckoview_version = "70.0.20190712095934"
}
```
c'est pour la version de geckoview et de canale de publication.
ensuite dans le bloc **dependencies** ajouter ça : 
```
implementation "org.mozilla.geckoview:geckoview-${geckoview_channel}:${geckoview_version}"
```

aussi dans le bloc **repositories** ajouter : 
```
 maven {
            url "https://maven.mozilla.org/maven2/"
        }
```

Ensuite
dans le fichier xml (activity_main.xml) vous trouverez une balize **TextView** qui est utilisée pour afficher du texte dans une interface utilisateur Android,  on a pas besoin de ça donc on la supprime.

on crée une autre balise pour injecter geckoview : 
```
   <org.mozilla.geckoview.GeckoView
        xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:tools="http://schemas.android.com/tools"
        android:id="@+id/geckoview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".MainActivity"/>
```
ce bloc de code XML définit une GeckoView qui s'étend sur toute la largeur et la hauteur de son parent  **LinearLayout**. L'ID geckoview peut être utilisé pour faire référence à cette vue dans le code Kotlin de l'activité MainActivity.

Enfin, maintenant on peut utiliser geckoview pour charger un **site web** sur une application android,
il suffit juste de coller l'url de site web que vous voulez mettre sous android.
avec cette ligne : 
```
        geckoSession.loadUri("https://firefox-source-docs.mozilla.org/mobile/android/geckoview/consumer/permissions.html") 
```

 on peut coller le site qu'on veut. par exemple moi j'ai pris le lien d'une  documentation sur la GeckoView.

voici le résultat quand on lance l'application: 

<div style="display: flex;">
    <img src="mark1.png" alt="Image 1" style="flex: 1;">    
</div>

# GeckoView et Extension Web
Notre objectif c'est de faire marcher l'extension MetaPress et non pas un site web, pour cela on peut utiliser la méthode  **WebExtensionController.installBuiltIn** de cette manière : 

```
runtime.getWebExtensionController()
  .installBuiltIn("resource://android/assets/messaging/")
  ```
  en remplaçant le dossier messaging avec notre extension qui se trouve dans le dossier **assets**.

  mais là on doit utiliser une version de geckoview plus récente, au début on a utilisé la version 70 (70.0.20190712095934) qui est sortie en 2019.

  pour faire fonctionner la méthode installBuiltIn() nous aurons besoin de la dérnière version qui est 127 (127.0.20240423214125) 

# Android Studio 2023
afin d'éviter tous conflits de version, nous allons installer la dernière version de logiciel android studio.
on peut l'obtenir via ce lien : 
https://developer.android.com/studio?hl=fr

mais dans cette version les choses sont différentes, dans les dernières versions d'Android Studio (à partir de la version 2022.2), le fichier activity_main.xml n'est plus créé par défaut pour les nouveaux projets Android.

La raison principale est l'introduction de Jetpack Compose, un framework d'interface utilisateur moderne pour Android. Jetpack Compose utilise une approche déclarative pour construire des interfaces utilisateur, éliminant ainsi le besoin de fichiers XML pour définir les mises en page.

A la place de activity_main.xml, vous définissez désormais l'interface utilisateur de votre activité principale en utilisant du code Kotlin dans la fonction **setContent**. Cette fonction utilise des composables Jetpack Compose pour construire l'arborescence de l'interface utilisateur de manière déclarative.

on va commencer à créer un nouveau projet en cliquant sur file -> new -> project 

un fichier en kotlin MainActivity.kt sera créé, il s'agit d'une application Android simple qui affiche le texte "hello Android !" à l'écran en utilisant Jetpack Compose pour une expérience de développement d'interface utilisateur moderne et déclarative.

j'ai essayé d'exécuter directement ça mais il y'a une erreur : 
<div style="display: flex;">
    <img src="erreur1.png" alt="Image 1" style="flex: 1;">    
</div>

que faire dans ce cas ? 

j'ai supprimé device qui était par défaut, et j'ai créé un autre comme suite : 

1. cliquer sur Create New Device
2. choisir la catégorie que vous voulez (phone,tablette,...) moi j'ai choisi phone
3. choisir le modèle (pixel fold,pixel 8 pro...)
4. ensuite next
5. choisir l'image de système (R,Q,tiramisu...)
6. ensuite next
7. choisir l'orientation (Portrait,Landscape)
8. cliquer sur finish

<div style="display: flex;">
    <img src="installer_sdk2.png" alt="Image 1" style="flex: 1;">    
</div>

dans device manager vous trouverez votre device que vous venez de créer, maintenant il reste qu'à ré-exécuter le projet.
et boom ça se lance 

<div style="display: flex;">
    <img src="sol2.png" alt="Image 1" style="flex: 1;">    
</div>

ok!

Maintenant la manière d'injecter geckoview dans notre nouvelle application android (MetaPress) est un peu différente, puisque on n'a pas les meme fichiers avec la version 2021.

d'abord on a un fichier libs.versions.toml qui est utilisé pour centraliser les versions des bibliothèques et des plugins dans un projet Gradle Kotlin. Cela permet de définir une seule fois les versions de ces éléments, puis de les référencer dans le fichier build.gradle.kts pour maintenir la cohérence et faciliter les mises à jour.

on va ajouter cette ligne a ce fichier en dessous de bloc [versions]: 
```
geckoviewNightly = "127.0.20240423214125"
```

ensuite dans le fichier build.gradle.kts on ajoute juste cette ligne : 
```
implementation (libs.geckoview.nightly)
```

on n'a pas besoin de déclarer deux variable pour channel et version comme on avait fait avant, au début j'ai fait ça mais un message de werrning apparait disant que tu n'a pas besoin de faire ça 

<div style="display: flex;">
    <img src="warn1.png" alt="Image 1" style="flex: 1;">    
</div>

dans le fichier settings.gradle ajouter cette ligne : 

```
maven {
            url = uri("https://maven.mozilla.org/maven2/")
        }
```

maintenat on peut utiliser geckoview. Mais quand j'ai essayé d'utiliser directement geckoview avec du code kotlin comme suite : 
```
                    GeckoWebView("https://firefox-source-docs.mozilla.org/mobile/android/geckoview/consumer/geckoview-quick-start.html")
```

l'application ça se lance pas, tout marche bien mais l'application au bout de 3 à 4 seconde elle se ferme avec ce message :
<div style="display: flex;">
    <img src="stoping.png" alt="Image 1" style="flex: 1;">    
</div>

quand je suis rentré à l'application dans l'émulateur, j'ai remarqué qu'il y'a un problème de permession, et dans ce cas il faut ajouter cette ligne dans le fichier **AndroidManifest.xml**

```
    <uses-permission android:name="android.permission.INTERNET" />
```

pour avoir les permession.

et quand on ré-exécute, voilà ça marche
<div style="display: flex;">
    <img src="marche.png" alt="Image 1" style="flex: 1;">    
</div>

maintenant on va essayé de faire marcher notre extension web, 
pour cela j'ai créé un dossier assets dans la racine du projet (dans main/), ensuite un autre dossier messaging
et dans ce dernier j'ai mis l'extension MetaPress.
j'ai fait un test avec : 
```
            geckoSession.loadUri("resource://android/assets/messaging/html/index.html")
```
j'ai pointé sur le fichier index.html qui se trouve dans le dossier html, donc le chemin complet est assets/messaging/html/index.html

et ça a marché: 
<div style="display: flex;">
    <img src="index.png" alt="Image 1" style="flex: 1;">    
</div>

maintenant la différence entre un site web et extension est que une extension il faut qu'elle soit installé d'abord pour qu'on puisse ensuite l'utiliser, on ne peut pas charger juste l'uri de l'extension.

pour cela on va installer l'extension avec **geckoRuntime.webExtensionController
            .installBuiltIn**

comme suite : 
```
 geckoRuntime.webExtensionController
            .installBuiltIn("resource://android/assets/messaging/meta-press-ext")
            .accept({
                Log.d("WebExtension", "Extension installed successfully")
            }, { error ->
                Log.e("WebExtension", "Error installing extension", error)
            })
````
j'ai changé aussi l'extension qui se trouve dans le dossier assets, cette fois ci j'ai pas fait juste un copier coller du projet mais un ````git clone --depth 1 --recurse-submodules https://framagit.org/Siltaar/meta-press-ext````

normalement jusqu'à maintenant l'extension est installée, il nous faut un moyen de vérifier ça.
pour cela il y'a quelques approches qu'on peut appliquer pour savoir si l'extension est installée ou non:

- j'ai ajouté des logs pour suivre l'avancement de l'installation
- j'ai essayé de chargé l'url **about:addons** pour lister les webExtensions qui sont installé
      ````
      geckoSession.loadUri("about:addons")
      ````
mais ça affiche une page blanche sans contenu
- j'ai essayé aussi avec debugging comme suite : 
````geckoSession.loadUri("about:debugging")```` mais meme chose, pas de résultat

- Meme chose avec about:about

une autre astuce pour vérifier si l'extension est installée est de chargé le site qui propose les extension à ajouter et voir si il propose toujours l'extension MetaPress ou il l'a considère comme etant installée !

pour faire ça : 
on charge ```` geckoSession.loadUri("https://addons.mozilla.org/fr/firefox/addon/meta-press-es/")````

et voici les résultats : 
<div style="display: flex;">
    <img src="Ajout.png" alt="Image 1" style="flex: 1;">    
</div>

il apparait qu'il propose toujours d'ajouter l'extension, 
mais on sait pas si cela veut dire que l'extension n'est pas installée réellement.

je peux aussi vérifier ça comme suite : 
j'ai ajouter une variable boolean que je l'initilise à false 
````private var isExtensionInstalled = false````,
ensuite lorsque on installe l'extension avec **installBuiltIn** on met à jour l'état de l'installation en mettant la variable isExtensionInstalled=true.

maintenant dans la méthode **setContent** qui est une fonction de la bibliothèque Android Jetpack Compose et qui permet de définir le contenu de l'activité, on vérifier d'abord si l'extension est installée avant d'essayer de l'afficher, cela revient à faire un "if" sur la variable "isExtensionInstalled".
````{
                    if (isExtensionInstalled) {
                      GeckoWebView(geckoSession)
                    } else {
                        InstallExtensionMessage()  
 ```` 

si elle est installée donc on peut la lancer, sinon on fait appel à une autre méthode **InstallExtensionMessage()**
qui permet d'afficher un message disant que l'extension n'est pas encore installée.

````@Composable
fun InstallExtensionMessage() {
    Text(
        text = "L'extension n'est pas installée. Veuillez installer l'extension pour continuer.",
        textAlign = TextAlign.Center,
        modifier = Modifier.fillMaxSize().padding(16.dp),
        //style = MaterialTheme.typography.body1
    )
}
````

et c'était le cas, quand on exécute le code  un message que j'ai définit dans InstallExtensionMessage apparait, donc on n'a pas encore réussi à installer l'extension.
<div style="display: flex;">
    <img src="extensionPasInstall.png" alt="Image 1" style="flex: 1;">    
</div>
dans ce cas on va vérifier les logs:

1. Première erreur que j'ai remarqué c'est le '/' à la fin de chemin de l'extension, moi avant je spécifie le chemin comme suite : 

````.installBuiltIn("resource://android/assets/meta-press-ext")````

mais en fait il faut ajouter le dernier '/' 
<div style="display: flex;">
    <img src="log1.png" alt="Image 1" style="flex: 1;">    
</div>

2. Deuxième erreur est que ça apparait que le fichier manifest.json n'est pas valide, peut etre il faut ajouter des trucs : 
<div style="display: flex;">
    <img src="log2.png" alt="Image 1" style="flex: 1;">    
</div>

la 1ere erreur est trop facile, maintenant il faut se concentrer plutot sur le fichier manifest.json

d'apres cette documentation : 
https://firefox-source-docs.mozilla.org/mobile/android/geckoview/consumer/web-extensions.html#assets-messaging-manifest-json

ils ont dit qu'il faut ajouter ces permissions :
geckoViewAddons, nativeMessaging, nativeMessagingFromContent

comme suite : 

 "permissions": [
    "nativeMessaging",
    "nativeMessagingFromContent",
    "geckoViewAddons"
  ]

  
  **geckoViewAddons :**
  Cette autorisation permet à notre extension d'accéder aux API GeckoView spécifiques aux extensions.
Elle est nécessaire pour que  l'extension puisse interagir avec GeckoView, telles que l'installation et la communication avec des extensions.

**nativeMessaging :**
Cette autorisation permet de communiquer avec des applications natives sur l'appareil.
Elle est nécessaire pour permettre à notre extension de communiquer avec l'application Android qui héberge GeckoView via des messages natifs.

**nativeMessagingFromContent:**
et celle là, elle  permet à l'extension d'envoyer des messages à partir de scripts de contenu (par exemple, JavaScript) vers des applications natives. (dans notre cas je vois pas pourquoi ajouter cette permission, mais puisque  dans la doc ils l'ont ajouté on va l'ajouter )

# autres logs
1.
<div style="display: flex;">
    <img src="invalide argument.png" alt="Image 1" style="flex: 1;">    
</div>

pour cette erreur j'avais aucune idée, j'ai tapé ça sur stackoverflow (https://stackoverflow.com/questions/53766789/failed-to-open-qemu-pipe-qemudnetwork-invalid-argument ) voici la réponse obtenue:

This error could be seen on Android Emulator/Device Version >= 6.0. It occurs when your app tries to send/receive request/response to a remote API that is not secure (http). That's the reason why you should stick to communicating with remote API's via a secured channel (https). But you can resolve the issue by setting the usesCleartextTraffic attribute to "true" inside the application opening tag of your android manifest file.

ils ont dit qu'il faut modifier le fichier android manifest qui se trouve dans android studioen changeant ça :

``` android:usesCleartextTraffic="true" ```
 
 le problème est réglé.

2.
<div style="display: flex;">
    <img src="browser_extension.png" alt="Image 1" style="flex: 1;">    
</div>

ici on voit que le script se charge uniquement dans un envirenement navigateur, pour cela il faut modifier le fichier browser-polyfill.min.js
pour qu'ilaccepte de fonctionner meme dans un envirenement android geckoview.

j'ai trouvé dans ce fichier une condition qui fait ça. elle vérifie si on est dans un envirenement browser ou non,moi je l'ai modifié comme suite : 
```
!function(e,r){"function"==typeof define&&define.amd?define("webextension-polyfill",["module"],r):"undefined"!=typeof exports?r(module):(r(r={exports:{}}),e.browser=r.exports)}("undefined"!=typeof globalThis?globalThis:"undefined"!=typeof self?self:this,function(e){"use strict";if(!isGeckoViewEnvironment()) throw new Error("This script should only be loaded in a browser extension or GeckoView environment.")
```
