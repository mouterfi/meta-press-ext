package com.example.myapplication

import android.support.v7.app.AppCompatActivity



import android.os.Bundle
import android.widget.TextView
import org.mozilla.geckoview.GeckoRuntime
import org.mozilla.geckoview.GeckoSession
import org.mozilla.geckoview.GeckoSessionSettings
import org.mozilla.geckoview.GeckoSessionSettings.USER_AGENT_MODE_MOBILE
import org.mozilla.geckoview.GeckoView

class MainActivity : AppCompatActivity() {

    private lateinit var geckoView: GeckoView
    private val geckoSession = GeckoSession()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val textView = findViewById<TextView>(R.id.textView)
        textView.text = "Hello mounir!"

        // Appeler la méthode setupGeckoView() à la fin de onCreate()
        setupGeckoView()
    }

    private fun setupGeckoView() {

        // 1. Récupérer la référence de GeckoView depuis le layout
        geckoView = findViewById(R.id.geckoview)

        // 2. Créer une instance de GeckoRuntime et ouvrir une session Gecko
        val runtime = GeckoRuntime.create(this)
        geckoSession.open(runtime)

        // 3. Définir la session Gecko sur le GeckoView
        geckoView.setSession(geckoSession)

        // 4. Charger l'URL dans la session Gecko
      //  geckoSession.loadUri("about:buildconfig")
       // geckoSession.loadUri("file:///android_asset/addons/")
       // geckoSession.loadUri("file:///android_asset/test.html")
                geckoSession.loadUri("https://www.meta-press.es/fr/")




    }

}
