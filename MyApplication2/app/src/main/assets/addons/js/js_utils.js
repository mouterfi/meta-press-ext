// SPDX-FileName: ./js_utils.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only

export const is = (val) => typeof val !== 'undefined'
export const is_str = (val) => typeof val === 'string'
export const is_obj = (val) => typeof val === 'object'
export const is_nb	= (val) => typeof val === 'number'
export const is_arr = (val) => Array.isArray(val)
export const is_in	= (lst, val) => lst.indexOf(val) !== -1
/**
 * Returns true if the given array contains the given value
 * @param {Array} array work space
 * @param {*} value The value to remove
 * @returns {Boolean} True if the array contains the value
 */
export function array_contains(arr, val) {
	return arr.indexOf(val) > -1
}
/**
 * Remove the 1st occurrence of the value in the given array.
 * @param {Array} array work space
 * @param {*} value The value to remove
 * @returns {Array} without value
 */
export function array_remove_once(arr, val) {
	let idx = arr.indexOf(val)
	if (idx > -1) arr.splice(idx, 1)
}
/**
 * Add a value in an Array only if it's not already in.
 * @param {Array} Array work space
 * @param {*} value The value to add
 * @returns {Array} The entry Array that might have been modified
 */
export function array_add_once(arr, val) {
	if(arr.indexOf(val) === -1) arr.push(val)
	return arr	// should be removed
}
/**
 * Removes duplicata in a array.
 * @param {Array} array The working array
 * @returns A new clean array
 */
export function remove_duplicate_in_array(arr) { return [...new Set(arr)] }
/*
 *
 */
export function shuffle_array(arr) {
	for (let i = arr.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[arr[i], arr[j]] = [arr[j], arr[i]]
	}
}
/** Returns a deep copy of an object, via JSON.parse(JSON.stringify(obj))
 * @param {Object} obj The object to duplicate
 * @returns {Object} a clone of the given object
 */
export function deep_copy(obj){ return JSON.parse(JSON.stringify(obj)) }
/**
 * Get the value at the given path of a JSON JSON object
 * @param {string} JSON_path The JSON path like : sources.invidius.tags.name
 * @param {Object} JSON_obj The object to search in
 * @returns The found value or undefined
 */
export function deref_JSON_path(JSON_path, JSON_obj) {
	let val = JSON_obj
	if (JSON_path)
		for (let JSON_path_elt of JSON_path.split(' -> ')) {  // all char is valid JSON key char
			// dailytelegraph.com.au is using '.' in keys
			// others are using '/'
			// lenouvellist.ch : 'aio:urls'
			// if '->' is colliding with a valid key, we can try : ':{' (a JSON object aperture)
			try {
				val = val[JSON_path_elt]
			} catch (exc) {
				return ''
			}
		}
	return val
}
/**
 * Format the given string to remove thousand separators from integers (so US ',' or Fr '.'/' ')
 * @param {string} str The string to be converted to Number()
 * @returns the string without any /,./g
 */
export function do_int (str) { return Number(str ? str.replace(/[,.\s]/g, '') : 0) }
/**
 * Generate a UUIDv4 (Unique Universal Identifier version 4)
 * @returns {string} UUIDv4
 */
export function uuidv4() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	)
}
/**
 * Encode the given XML string with UTF8 entities
 * More info : https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
 * @param {string} s String to encode
 * @example XML_encode_UTF8 (" « ") returns " &#171; "
 * @returns {string} The encoded string
 */
export function XML_encode_UTF8 (s) {
	return s.replace(/[&\u00A0-\u9999]/g, (i) => `&#${i.charCodeAt(0)};`)
}
/**
 * Encode the string for xml (<, >, &, ', ").
 * @param {string} unsafe The string to encode
 * @returns {string} The encoded string
 */
export function encode_XML(unsafe) {	// used in export_XML
	unsafe = removeXMLInvalidChars(unsafe, false) // removeDiscouragedChars -> removes all
	return unsafe.replace(new RegExp('[<>&\'"]', 'g'), (c) => {
		switch (c) {
		case '<': return '&lt;'
		case '>': return '&gt;'
		case '&': return '&amp;'
		case "'": return '&apos;'
		case '"': return '&quot;'
		}
	})
}
/**
 * Removes invalid XML characters from a string
 * From https://gist.github.com/john-doherty/b9195065884cdbfd2017a4756e6409cc
 * @param {string} str - a string containing potentially invalid XML char. (non-UTF8, STX, EOX)
 * @param {boolean} removeDiscouragedChars - should it remove discouraged but valid XML char.
 * @return {string} a sanitized string stripped of invalid XML characters
 */
function removeXMLInvalidChars(str, removeDiscouragedChars=true) {
	// remove everything forbidden by XML 1.0 specifications, plus the unicode replacement
	// character U+FFFD
	/* eslint-disable no-control-regex */
	let regex = new RegExp('[^\x09\x0A\x0D\x20-\xFF\x85\xA0-\uD7FF\uE000-\uFDCF\uFDE0-\uFFFD]',
		'gm')
	/* eslint-enable no-control-regex */
	str = str.replace(regex, '')
	if (removeDiscouragedChars) {
		// remove everything discouraged by XML 1.0 specifications
		console.debug('Using removeDiscouragedChars')
		console.trace()
		regex = new RegExp(
			'([\\x7F-\\x84]|[\\x86-\\x9F]|[\\uFDD0-\\uFDEF]|(?:\\uD83F[\\uDFFE\\uDFFF])|' +
			' (?:\\uD87F[\\uDFFE\\uDFFF])|(?:\\uD8BF[\\uDFFE\\uDFFF])|(?:\\uD8FF[\\uDFFE\\uDFFF])|'+
			'(?:\\uD93F[\\uDFFE\\uDFFF])|(?:\\uD97F[\\uDFFE\\uDFFF])|(?:\\uD9BF[\\uDFFE\\uDFFF])|'+
			'(?:\\uD9FF[\\uDFFE\\uDFFF])|(?:\\uDA3F[\\uDFFE\\uDFFF])|(?:\\uDA7F[\\uDFFE\\uDFFF])|'+
			'(?:\\uDABF[\\uDFFE\\uDFFF])|(?:\\uDAFF[\\uDFFE\\uDFFF])|(?:\\uDB3F[\\uDFFE\\uDFFF])|'+
			'(?:\\uDB7F[\\uDFFE\\uDFFF])|(?:\\uDBBF[\\uDFFE\\uDFFF])|(?:\\uDBFF[\\uDFFE\\uDFFF])|'+
			'(?:[\\0-\\t\\x0B\\f\\x0E-\\u2027\\u202A-\\uD7FF\\uE000-\\uFFFF]|[\\uD800-\\uDBFF]'+
			'[\\uDC00-\\uDFFF]|[\\uD800-\\uDBFF](?![\\uDC00-\\uDFFF])|(?:[^\\uD800-\\uDBFF]|^)'+
			'[\\uDC00-\\uDFFF]))', 'g')
		str = str.replace(regex, '')
	}
	return str
}
/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
 * $& means the whole matched string
 */
export function escape_RegExp(string) { return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') }
/**
 * Cut the string to the at size if string is too long.
 * @param {string} str The string
 * @param {number} at The needed size
 * @returns {string} the shorten str
 */
export function shorten(s, at) { return s && s.length > at ? `${s.slice(0, at)}…` : s || '' }
/**
 * Remove trailing space before and after the string and the double space (\s\s) in the string.
 * @param {string} str Working string
 * @returns {string}
 */
export function triw(str) { return str && str.replace(/^\s*|\s*$|(\s\s)+/g, '') || '' }
/**
 * Remove all spaces.
 * @param {string} str Working string
 * @returns {string}
 */
export function dry_triw(str) { return str && str.replace(/\s*/g, '') }
export function drop_low_unprintable_utf8(str) {
	str = str.replace(/[\u0000-\u0009]/g, '')					 // eslint-disable-line no-control-regex
	str.replace(/[\u000B-\u000C]/g, '')								 // eslint-disable-line no-control-regex
	return str && str.replace(/[\u000E-\u001F]/g, '')  // eslint-disable-line no-control-regex
}
/**
 * Quote the special character in the string.
 * From : http://kevin.vanzonneveld.net, http://magnetiq.com
 * @param {string} str Working string
 * @example preg_quote("How many? $40") returns 'How many\? \$40'
 * @example preg_quote("\\.+*?[^]$(){}=!<>|:"); returns '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'
 * @returns {string} The string prepared
 */
export function preg_quote(str) {
	// str = String(str).replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1")
	str = String(str).replace(/([\\.+*?[^\]$(){}=!<>|:])/g, '\\$1')
	return str.replace(/\s/g, '\\s')	// ensure we match converted &nbsp;
}
export function bolden (str, search) {
	if (typeof str !== 'string') return ''
	let search_term_list = search.split(' ')
	for (let i of search_term_list)
		str = str.replace(new RegExp(`(${preg_quote(i)})`, 'gi'), '<b>$1</b>')
	return str
}
/**
 * Quote the special character in the string for a file.
 * @param {string} str Working string
 * @returns {string} The string prepared
 */
export function preg_quote_file(str) { return str.replace(/([\\"',])/g, '\\$1') }
export function drop_escaped_quote_file(str) { return str.replace(/\\(["',\n])/g, '$1') }
/**
 * Remove the \' (and \") in a string.
 * @param {string} str The string to clean
 * @returns {string}
 */
export function drop_escaped_quotes(str) { return str.replace(/\\'/g, "'").replace(/\\"/g,'"') }
/**
 * Extract the domain of the given url.
 * @param {string} url The url
 * @returns {string} The domain extracted
 */
export function domain_part(url) {
	let [htt, , dom] = url.split('/')
	return `${htt}//${dom}`
}
/**
 * Extract the domain name of the given url.
 * @param {string} url The url
 * @returns {string} The domain extracted
 */
export function domain_name(url) {
	let [ , , dom] = url.split('/')
	let s_dom = dom.split('.')
	if (s_dom[0] === 'www') s_dom = s_dom.slice(1, s_dom.length)
	return title_case(s_dom.join('.'))
}
// const valid_HTTP_protocols = ['https:', 'http:', 'data:', 'file:']

/**
 * String to define a regular expression test the validity of a URL
 * https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
 * https://www.freecodecamp.org/news/check-if-a-javascript-string-is-a-url/
 * https://stackoverflow.com/questions/13373504/what-is-a-valid-url-query-string
 * https://www.rfc-editor.org/rfc/rfc3986
 * http://tools.ietf.org/html/std66
 */
const rfc3986_alphanum_subdelim = '-a-z0-9._~!$&"\'()*+,;=:@'
const encoded_hexa = '%[0-9a-f]{2}'
const rfc3986_allowed_in_path = `[${rfc3986_alphanum_subdelim}]|${encoded_hexa}`
const rfc3986_allowed_in_querystring = `[${rfc3986_alphanum_subdelim}/?]|${encoded_hexa}`
const valid_HTTP_URL_str = '^\\s*'+
	'(https?://)?'+ // validate protocol
	'('+
		'('+
			'('+
				'(www\\.[\\w-.]*?[\\w-])'+ // validate sub-domain and domain name with www.
				'|((?!www\\.)[\\w.-]*?[\\w-])'+ // validate sub- and domain name, negative lookahead
			')'+
			'\\.[a-z]{2,}'+ // validate extension
		')'+
//		'|((\\d{1,3}\\.){3}\\d{1,3})'+ // or validate IPv4 address
//    '|()'+  // or validate IPv6 address hint: https://gist.github.com/syzdek/6086792
	')'+
	'(:\\d{1,5})?'+ // validate port 0 - 65536
	`(/(${rfc3986_allowed_in_path})*)*`+ // validate path
	`(\\?(${rfc3986_allowed_in_querystring})*)?`+ // validate query string
	`(\\#(${rfc3986_allowed_in_querystring})*)?`+ // validate fragment locator / anchor
	'\\s*$'
/**
 * Regular expression to test the validity of a URL
 */
const valid_HTTP_URL_RE = new RegExp(valid_HTTP_URL_str, 'i')
/**
 * Tests if a given string is a valid URL
 * @param {string} str The string to test
 * @returns {boolean} URL validity
 */
/*export function is_valid_HTTP_URL(str) {  // inspired from https://stackoverflow.com/a/43467  144
	// let url
	try { url = new URL(str) } catch (_) { return false }
	return true  // valid_HTTP_protocols.includes(url.protocol)
}*/  // validates : http://w
export function is_valid_HTTP_URL(str) {
	return valid_HTTP_URL_RE.test(str)  // validates : http://www.kon
}
/**
 * Tranform the link in a url link for domain part.
 * @param {string} link The link to transform
 * @param {string} domain_part The doamin part of the destination
 * @returns {string} The working url
 */
export function urlify(link, domain_part) { // return URL from any href (even relative links)
	let url
	if (!link ||
		link.startsWith('http') ||
		link.startsWith('data:') ||
		link.startsWith('file:')
	)
		// and we are very happy to test implicitely many cases
		// http -> the link is ok ? might need to check for is_valid_HTTP_URL
		// data: -> e.g. img from JamaicaObserver, we need to have them pass through
		// should we also passthrouh file: links ?
		return link
	// else if (link.startsWith('file:') || link.startsWith('data:'))
	// remove MP auto-added path
		// what is the MP auto-added path ?
		// url = link.replace(document.URL.split('/').slice(0,-1).join('/'), domain_part)
	else if (link.startsWith('//'))
		url = (domain_part.split('/'))[0] + link
	else {
		// *.wikinews.org links are already encoded
		// one source (which one ?) needs links to be encoded
		let path = is_in(link, '%') ? link : encodeURI(link)
		let sep = link[0] !== '/' ? '/' : ''
		url = `${domain_part}${sep}${path}`
	}
	if (!is_valid_HTTP_URL(url)) {
		console.error('urlify detected an invalid URL')
		console.debug('urlify input link', {link})
		console.debug('urlify url', {url})
	}
	return is_valid_HTTP_URL(url) ? url : null
}
/**
 * Return a string formated in title case : 1st letter uppercase and others lowercase.
 * @function
 * @param {string} str string to tranform
 * @returns {string} string title formated
 */
export function title_case(str) {
	return str.replace(/\w\S*/g, a => `${a.charAt(0).toUpperCase()}${a.substr(1).toLowerCase()}`)
}
/**
 * Return a 'random' number of n digits
 * @function
 * @param {number} n The number of wanted digits in the returned integer
 * @returns {number} A 'random' integer of given n digits
 */
export function rnd(n) { return Math.ceil (Math.random () * Math.pow (10, n)) }
/**
 * Pick a 'random' number between a and b.
 * @function
 * @param {number} a Inferior limit
 * @param {number} b Superior limit
 * @returns {number} The random number
 */
export function pick_between(a, b) { return Math.floor(Math.random() * b) + a }
/**
 * Extract the given regex matching group(s) and return it (them)
 * @param {string} re The matching groups of the regular expression to apply, everything before
 * and after them will be matched outside replacement groups and discarded.
 * @param {string} str The string to search in
 * @param {string} repl The replacement tokens used to keep some matching groups (default : $1)
 * @param {string} p regular expression string that matches the smallest
 * ensemble of every possible characters, not creating a match group
 * @returns {string} the input string with tokens replaced
 */
export function regextract (re, str, repl='$1', p='(?:.*?)') {
	str = str_uniq_line(str)
	let str_rep = str.replace(new RegExp(`^${p}${re}${p}$`), repl)
	return str_rep !== str ? str_rep : ''  // if the match/replace failed, return an empty string
}
/**
 * Concat all lines of a single String into a one liner String
 * @param {string} str The string to work on
 * @returns {string} a new string with no '\n' nor '\r' in it
 */
export function str_uniq_line (str) {
	return str.replace(/[\n\r]+/g, '')
}
/**
 * Format the string with tokens : $1 -> $i.
 * @example str_fmt('$1-ok', ["ko"]) return 'ko-ok'
 * @param {string} a The string to format
 * @param {Array} tokens The tokens to use
 * @returns {string} The formated string
 */
export function str_fmt (str, tokens) {
	// console.log('str_fmt', str, tokens)
	for (let [k, v] of Object.entries(tokens)) {
		str = str.replace(new RegExp(`\\$${Number(k)+1}`, 'g'), String(v || ''))
	}
	return str
}
/**
 * Generate string of the current date in a generic human readable format
 * @returns {string} Human readable current date representation
 */
export function ndt_human_readable() {
	return new Date().toISOString().replace('T', '_').replace(':', 'h').split(':')[0]
}
/**
 * Await for a certain delay, in milliseconds
 * From : https://www.delftstack.com/howto/javascript/javascript-wait-for-x-seconds/#use-promises-and-async%2fawait-to-wait-for-x-seconds-in-javascripthttps://www.revue.lu/search/
 * @function
 * @param {Number} n the delay in ms
 * @returns {Promise}
 */
export async function delay(n) {
	return new Promise((resolve) => setTimeout(resolve, n))
}
export function slugify(str) { return str.replace(/ /g, '_') }
export function rm_trailing_slash(str) { return str.replace(/\/$/, '') }
/**
 * Get the selected value in the given HTML select object
 * @param {Object} sel The HTML select object
 * @returns The current selected value
 */
export function get_HTML_select_value(sel) { return sel.options[sel.selectedIndex].value }
/**
 * Remove an anchor from a given URL.
 * @example  rm_href_anchor('olivier.fr#tree') return 'olivier.fr'
 * @param {URL} permalink The link
 * @returns The link without the anchor
 */
export function rm_href_anchor(permalink) { return permalink.href.split('#')[0] }
/**
 * Check if the given HTML element is in overflow status
 * @param {Node} elt The HTML element to check
 * @returns {boolean} overflow ?
 */
export function isOverflown(elt) {
	return elt.scrollHeight > elt.clientHeight || elt.scrollWidth > elt.clientWidth
}
