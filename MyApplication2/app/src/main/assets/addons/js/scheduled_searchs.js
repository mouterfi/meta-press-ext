// SPDX-FileName: ./scheduled_searchs.js
// SPDX-FileCopyrightText: 2022-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
/* globals browser */
import * as mµ from './mp_utils.js'
import * as mp from './mp_core.js'

/**
 * @module scheduled_searchs
 */
/**
 * Schedulded searchs.
 * @namespace scheduled_searchs
 */

const dbg = true

export const run_frequencies = [
	'sch_s_hourly',
	'sch_s_daily',
	'sch_s_weekly',
	'sch_s_monthly',
	'sch_s_stop'
]

/**
 * Loads the scheduled search from the browser storage.
 * @function init_table_sch_s
 * @memberof scheduled_searchs
 * @param {Object} browser_str The browser storage
 * @returns {Array} the list of shedule search
 */
export function init_table_sch_s(browser_str) {
	let table_sch_s = []  // init value of table_sch_s
	let keys_elt_browser_str = Object.keys(browser_str)//get all keys of object in browser storage
	let regex = /sch_sea__/
	for(let i of keys_elt_browser_str) {// for all keys
		if(i.match(regex)) {//if keys match with 'sch_sea__'
			let id = i.split('__')[1]
			table_sch_s[id] = browser_str[i]//add entries in table_sch_s
		}
	}
	return table_sch_s
}
/**
 *
 */
export function set_timezone(sch_sea_date, tz, nav_to_tz=true) {
	if (sch_sea_date && sch_sea_date !== 0)
		return mp.timezoned_date(sch_sea_date, tz, nav_to_tz)
	return sch_sea_date
}
/**
 * Shedules a new automated search
 * @memberof scheduled_searchs
 * @param {Date} date The date of the ancient search
 * @param {string} frq The frequencies of the search sch_s_stop sch_s_hourly sch_s_daily
 * sch_s_weekly sch_s_monthly sch_s_quaterly sch_s_half-yearly sch_s_annual
 * @returns The date of execute search.
 */
export function sch_sea_maj_date(date, frq, tz) {
	// const cur_date = set_timezone(new Date(), tz)
	const cur_date = mp.timezoned_date(new Date(), tz, true)
	if ('sch_s_stop' === frq)
		return cur_date
	// date = set_timezone(new Date(date), tz)
	date = mp.timezoned_date(date, tz, true)
	if (isNaN(date))
		return console.error(`Bad date: ${date}`)
	let d = new Date(date)
	if(cur_date >= date) {
		if(frq === 'sch_s_hourly' || frq === 'sch_s_daily' || frq === 'sch_s_weekly') {
			d = cur_date
			if(frq === 'sch_s_hourly') {
				d.setMinutes(date.getMinutes())
				if(d <= cur_date)
					d.setTime(d.getTime()+3600000)
			} else
				if(frq === 'sch_s_daily') {
					d.setMinutes(date.getMinutes())
					d.setHours(date.getHours())
					if(d <= cur_date)
						d.setTime(d.getTime()+86400000)
				} else if(frq === 'sch_s_weekly') {
					d.setMinutes(date.getMinutes())
					d.setHours(date.getHours())
					let mod = (7 - cur_date.getDay() + date.getDay()) % 7
					d.setTime(d.getTime()+(86400000*mod))
					if(d <= cur_date)
						d.setTime(d.getTime()+(86400000*7))
				}
		} else
			if(frq === 'sch_s_monthly' || frq === 'sch_s_quarterly' || frq === 'sch_s_half-yearly') {
				d.setMonth(cur_date.getMonth())
				d.setFullYear(cur_date.getFullYear())
				if(frq === 'sch_s_monthly') {
					if(d < cur_date)
						d = date.setMonth(date.getMonth() + 1)
				} else
					if(frq === 'sch_s_quarterly') {
						if(!(d.getMonth() - date.getMonth()) % 3) {
							let mod = (3 - (d.getMonth() - date.getMonth()))%3
							d.setMonth(d.getMonth() + mod)
						}
						if(d < cur_date)
							d = date.setMonth(date.getMonth() + 3)
					} else if(frq === 'sch_s_half-yearly') {
						if(!(d.getMonth() - date.getMonth()) % 6) {
							let mod = (6 - (d.getMonth() - date.getMonth())) % 6
							d.setMonth(d.getMonth() + mod)
						}
						if(d < cur_date)
							d = date.setMonth(date.getMonth() + 6)
					}
				if(d.getMonth() < cur_date.getMonth())
					d.setFullYear(cur_date.getFullYear() + 1)
			} else if(frq === 'sch_s_annual') {
				d = date.setFullYear(cur_date.getFullYear())
				if(d < cur_date)
					d = date.setFullYear(date.getFullYear() + 1)
			}
	}
	if (d < cur_date || isNaN(d)) {
		console.error('Failed to compute scheduled search next run date', d)
		return
	}
	return d
}
function full_sch_s_url(sch_s) {
	let local_pseudo_domain = new URL(window.location).origin
	return new URL(`${local_pseudo_domain}/html/index.html${sch_s}`)
}
/**
 * Launches a search and shedulde a new one if needed.
 * @memberof sch_search
 * @async
 * @param {Object} sch_s The search to launch
 */
export async function launch_sch_s(sch_s) {
	// sch_s = sch_s.name ? sch_s.name : sch_s
	dbg && console.log(`launch_sch_s : ${sch_s}`)
	sch_s = sch_s.name || sch_s
	let new_url = full_sch_s_url(sch_s)
	let run_freq = new_url.searchParams.get('run_freq')
	if (!run_frequencies.includes(run_freq))
		return console.warn(`Bad run_freq : ${run_freq} for ${new_url}`)
	let next_run = new_url.searchParams.get('next_run')
	let index = new_url.searchParams.get('id_sch_s')
	new_url.searchParams.set('last_run', new Date().toUTCString())
	next_run = sch_sea_maj_date(next_run, run_freq)
	new_url.searchParams.set('next_run', next_run.toUTCString())
	mµ.to_storage(index, new_url.search)
	// if(run_freq !== 'sch_s_stop')
	new_url.searchParams.set('submit', '1')
	await browser.tabs.create({ 'url': new_url.toString() })
	create_alarm(new_url.search)
}

export function create_alarms (arr_sch_s) {
	if (!Array.isArray(arr_sch_s))
		console.error(`arr_sch_s: ${arr_sch_s} ; should be an Array`)
	for (let sch_s of arr_sch_s)
		create_alarm (sch_s)
}
/**
 * Creates a new alarm for the given shedulded search(s).
 * @memberof sch_search
 * @param {(string)} sch_s The schedulded search(s)
 */
export function create_alarm (sch_s) {
	let new_url = full_sch_s_url(sch_s)
	let run_freq = new_url.searchParams.get('run_freq')
	if (!run_frequencies.includes(run_freq))
		return console.warn(`Bad run_freq: ${run_freq} ; for: ${sch_s}`)
	let next_run = new_url.searchParams.get('next_run')
	let next_run_date = new Date(next_run)
	if (isNaN(next_run_date))
		return console.warn(
			`Failed to parse next run date: ${next_run} ; for: ${sch_s}`)
	if(run_freq !== 'sch_s_stop') {
		next_run_date = next_run_date.getTime()
		browser.alarms.create(sch_s, {when: next_run_date})
		browser.alarms.onAlarm.addListener(launch_sch_s)
	}
	console_debug_alarms()
}
/**
 * Logs the alarms use for scheduled searchs in the extension console if do_print is True.
 * @async
 * @memberof sch_search
 * @param {boolean} do_print display flag
 */
async function console_debug_alarms() {
	let alarms = await browser.alarms.getAll()
	// await browser.alarms.getAll().then(elt => {alarms = elt})
	console.log('Scheduled search stored browser.alarms:', alarms)
	let i = 0
	for(let al of alarms) {
		let date = new Date(al.scheduledTime)
		console.log("L'alarme ", i, ' se lancera le ', date)
		i += 1
	}
}
