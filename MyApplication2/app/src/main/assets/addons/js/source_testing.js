// SPDX-FileName: ./source_testing.js
// SPDX-FileCopyrightText: 2022-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
//
import * as _     from './js_utils.js'
import * as sµ 		from './source_utils.js'
import * as mp 		from './mp_core.js'
import { known_countries_and_code, locales } from './BOM_utils.js'


/**
 * @module source_testing
 */



/**
 * All the function for test (format, unit).
 * @namespace source_testing
 */

export function edit_style_class(elt, err_status) {
	if (!err_status) throw 'no err_status ' + window.location.pathname
	elt.classList.remove('default','success','warn','error','no_result_error', 'network_error')
	if(Array.isArray(err_status))
		for(let i of err_status)
			elt.classList.add(i)
	else
		elt.classList.add(err_status)
}
/**
 *
 */
export function add_result_test_to_query(src_def, elt, txt, query, val, err_status) {
	txt = txt.toString()
	let local_url = new URL(window.location)
	local_url.pathname = '/html/index.html'
	local_url.searchParams.delete('test_sources')
	local_url.searchParams.set('tech', 'cherry-pick_sources')
	local_url.searchParams.set('add_src_sel', elt.dataset.src_key)
	local_url.searchParams.set('q', query)
	local_url.searchParams.set('submit', '1')
	let div_q, link_q
	if(elt.querySelector(`[data-query='${query}']`) !== null) {
		div_q = elt.querySelector(`[data-query='${query}']`)
		link_q = div_q.getElementsByTagName('a')[0]
		link_q.textContent = `${val} ${query} [MP ↗] `
	} else {
		div_q = document.createElement('div')
		div_q.dataset.query = query  // 1st case of dataset usb
		elt.appendChild(div_q)
		link_q = document.createElement('a')
		link_q.textContent = query + ' [MP ↗] '
		link_q.target = '_blank'
		link_q.href = local_url
		div_q.appendChild(link_q)
		link_q = document.createElement('a')
		link_q.textContent = '[web ↗]'
		link_q.target = '_blank'
		link_q.href = mp.format_search_url(sµ.get_search_url(src_def), query, 10)
		div_q.appendChild(link_q)
	}
	edit_style_class(link_q, err_status)
	let p = document.createElement('p')
	p.textContent = txt
	div_q.appendChild(p)
}
/**
 *
 */
export function no_err(src_def, src_key, msg, dest, doc) {  // dest is always 'test_src'
	let local_url = new URL(window.location)
	let query = local_url.searchParams.get('q')
	let res_btn = doc.getElementById(`btn_${src_key}`)
	/*if(!res_btn.classList.contains('error') &&
		!res_btn.classList.contains('warn') &&
			!res_btn.classList.contains('no_result_error')
	) {
		edit_style_class(res_btn, 'success')
		res_btn.textContent = 'V'
	}*/
	edit_style_class(res_btn, 'success')
	res_btn.textContent = 'V'
	let res_link = doc.querySelector(`[id='report_${src_key}']>.${dest}>[data-query='${query}']`)
	if(!res_link.classList.contains('error') &&
		!res_link.classList.contains('warn') &&
		!res_link.classList.contains('no_result_error')
	) {
		let report_frame = doc.querySelector(`[id='report_${src_key}']>.${dest}`)
		add_result_test_to_query(src_def, report_frame, msg, query, 'V', 'success')
	}
}
/**
 *
 */
export function add_err(src_def,	err_code, txt, err_status, dest, doc=document) {
	let src_key = src_def.tags.key
	console[err_status](src_key, txt)
	console.trace()
	let local_url = new URL(window.location)
	let query = local_url.searchParams.get('q')
	let val
	let elt_btn = doc.getElementById(`btn_${src_key}`)
	let report_frame = doc.querySelector(`[id='report_${src_key}']>.${dest}`)
	let res_card_btn = doc.querySelector(`[id='report_${src_key}'] .${dest}_btn`)
	if (!elt_btn)  // when called from a normal search
		return
	if(dest === 'test_src') {
		switch(err_code) {
		case sµ.error_code.NO_RESULT: val = '0' ;break
		case sµ.error_code.NETWORK_ERROR: val = 'N'
			err_status = 'network_error'
			edit_style_class(elt_btn, err_status) ;break
		default: val = 'X'
		}
		let report_peas = report_frame.querySelectorAll(`[data-query='${query}'] > p`)
		if (report_peas.length && report_peas[0].textContent == 'No error') {
			// why is there an empty "p" on top sometimes
			err_status = sµ.error_status.WARNING
			val = 'W'
		}
		if (_.is_str(txt) && txt.length > 0 && src_def.tags.warn_mask) {
			let is_masked = txt.replace(new RegExp(src_def.tags.warn_mask), '')
			if (is_masked.length == 0) {
				err_status = sµ.error_status.WARNING
				val = 'M'
			}
		}
		// below it 'write the result'
		edit_style_class(report_frame, `${err_status}_`)
		elt_btn.textContent = val
		elt_btn.classList.add(err_code)
		add_result_test_to_query(src_def, report_frame, txt, query, val, err_status)
	} else if(dest === 'test_integrity') {
		elt_btn.textContent = 'I'
		let p = document.createElement('p')
		p.dataset.error =	sµ.error_code
		p.textContent = txt
		report_frame.appendChild(p)
		edit_style_class(p, err_status)
	} else throw `unknow add_err dest ${dest}`
	if(!elt_btn.classList.contains('error') && !elt_btn.classList.contains(err_status))
		edit_style_class(elt_btn, err_status)
	edit_style_class(res_card_btn, err_status)
	if (res_card_btn.getElementsByClassName('fld_ico')[0].classList.contains('fld_ico_hide'))
		res_card_btn.click()  // open it only once
}
/**
 *
 */
export function add_errors(all_error, doc) {
	for(let error of all_error)
		add_err(error.src_def, error.code, error.text, error.level, error.dest, doc)
}

/**
 * The boolean describes if a source is correct by testing its attributes
 * {@link test_attribute}.
 * @memberof test
 */
// let def_attr_is_complete = true
/**
 * The list of the all error found by {@link test_attribute}.
 * @memberof test
 */
let error_attr = []

/**
 * Tests if a source URL is correct and reachable.
 * @memberof test
 * @param {Object} src - the src to test (for get the name)
 * @param {string} url - URL to testget_current_source_search_hosts
 * @param {string} attr_name - name, key of the attribute
 * @param {Object} status - object who contain code in key and true in value
 * {200: true,300: true}
 * @returns none
 */
export function test_url(src, url, attr_name, status) {
	if (_.is(src[attr_name]) && src[attr_name] !== '') {
		let requete = new XMLHttpRequest()
		requete.open('GET', url, false)
		try {
			requete.send()
			if (!status[requete.status] && requete.status !== 304) {
				// def_attr_is_complete = true
				error_attr.push(
					sµ.create_error(
						src,
						sµ.error_code.NETWORK_ERROR,
						`${attr_name} ${url} is not correctly receive ${requete.status} code`,
						'warn',
						'test_integrity',
						{ test: 'url', 'url': url, 'status': status.toString() }
					)
				)
			}
			return 200
		} catch (exec) {
			console.error(exec)
			// def_attr_is_complete = true
			error_attr.push(
				sµ.create_error(
					src,
					sµ.error_code.ERROR,
					`${attr_name} is not correctly receive... ${requete.status} code`,
					'warn',
					'test_integrity',
					{ test: 'url', 'url': url, 'status': status.toString() }
				)
			)
		}
	}
	return
}

export const SRC_DEF_TESTS = {
	'base': [
		'favicon_url',
		'search_url',
		'tags',
		'tags.lang',
		'tags.country',
		'tags.tech',
		'tags.themes',
		'tags.src_type',
		'tags.res_type'],
	'rss': {},
	'rss_url': [
		'/search/{}/feed?orderby=post_date&order=desc',
		'/search/{}/feed2?orderby=post_date&order=desc',
		'/search.rss?q={}',
		'/search/{}/feed.xml',
		'?s={}&feed=rss2'
	],
	'extends': {
		'test': ['inSource']
	},
	'favicon_url': {
		'test': ['url'],
		'url_code': 200
	},
	'type': {
		'test': ['oneofvalues'],
		'test_oneofvalues': ['JSON', 'XML']
	},
	'json_to_html': {
		'test_conflict': ['type']
	},
	'news_rss_url': {
		'test': ['url'],
		'url_code': 200
	},
	'search_url': {
		'test': ['re'],
		'test_re': '{}',
		'test_level': 'warn'
	},
	'search_url_web': {
		'test': ['re'],
		'test_re': '{}',
		'test_conflict': ['body'],
		'test_level': 'warn'
	},
	'method': {
		'test': ['oneofvalues'],
		'test_oneofvalues': ['POST']
	},
	'body': {
		'test': ['re'],
		'test_re': '{}'
	},
	'redir_url': {
		'test': ['url'],
		'url_code': 200
	},
	'domain_part': {
		'test': ['url'],
		'url_code': 200
	},
	'results': {
		'com': 'this object needs to exists, even empty'
	},
	'r_h1': {
		'conflicts': ['r_h1_xpath']
	},
	'r_url': {
		'conflicts': ['r_url_xpath']
	},
	'r_dt': {
		'conflicts': ['r_dt_xpath']
	},
	'r_txt': {
		'conflicts': ['r_txt_xpath']
	},
	'r_by': {
		'conflicts': ['r_by_xpath']
	},
	'tags': {},
	'tags.lang': {
		'test': ['oneofvalues'],
		'test_oneofvalues': integrity_locales()
	},
	'tags.country': {
		'test': ['oneofvalues'],
		'test_oneofvalues': Object.keys(known_countries_and_code('en')) || []
	},
	'tags.themes': {},
	'tags.tech': {},
	'tags.src_type': {},
	'tags.res_type': {
		'test': ['oneofvalues'],
		'test_oneofvalues': ['audio', 'image', 'text', 'video', 'event', 'job'],
		'val_can_be_array': true
	},
	'tags.tz': {
		'test': ['oneofvalues'],
		'test_oneofvalues': Intl.supportedValuesOf && Intl.supportedValuesOf('timeZone') || []
	}
}
/**
 *
 */
function integrity_locales() {
	let l = locales()
	l = l && l.concat([
		'oc',  // https://iso639-3.sil.org/code/oci / oc Occitan
		'tet', // https://iso639-3.sil.org/code/tet Tetum
		'tl',  // https://iso639-3.sil.org/code/tgl / tl Tagalog
		'sh',  // https://iso639-3.sil.org/code/hbs / sh Serbo-Croatian
		'xnz', // https://iso639-3.sil.org/code/xnz Kenzi
		'aym', // https://iso639-3.sil.org/code/aym Aymara
		'arz', // https://iso639-3.sil.org/code/arz Egyptian Arabic
		'scn', // https://iso639-3.sil.org/code/scn Sicilian
		'ckb', // https://iso639-3.sil.org/code/ckb Central Kurdish
	]) || []
	return l
}
/**
 * Tests the regular expression for the attr_name of src.
 * @memberof test
 * @param {Object} src_def_tests - The object who contain the description of the format
 * (test_integrity.json)
 * @param {Object} src - The object describing the source
 * @param {string} attr_name - The name (key) of the attribute
 * @param {string} regex - The regex to test the attribute name
 * @returns {Boolean} If the regex match
 */
function test_re(src, attr_name, regex) {
	// let attr = SRC_DEF_TESTS[attr_name]
	let error_level = 'error'
	if (!regex.test(src[attr_name])) {
		/*if (attr.test_level) {
			def_attr_is_complete = true
			error_level = attr.test_level
		}*/
		error_attr.push(
			sµ.create_error(
				src,
				sµ.error_code.INVALID_VALUE,
				`${src[attr_name]} is an invalid ${attr_name} (${regex})`,
				error_level,
				'test_integrity',
				{ test: 'regex',	'regex': regex.toString(), data: src[attr_name]	}
			)
		)
		return false
	}
	return true
}
/**
 * Tests if the attribute is well formed with the given file-described format.
 * @memberof test
 * @param {Object} sources - The sources.json build object
 * @param {Object} src_def_tests - The test_integrity.json build object
 * @param {Object} src - The source object to test
 * @param {string} attr_name - The name, key of attribute to test
 * @param {boolean} slow - Test with request
 * @param {string} dependent - The string who describe the dependecies of an attributes
 * @returns {Array} Who contain in first a Boolean the validity of the attribute, and the error
 * array [[error, name, level], ...]
 */
export function test_attribute(sources, src, attr_name, slow, dependent='none') {
	let error_attr = []
	let def_attr_is_complete = true
	let local_src = _.deep_copy(src)
	let attr = SRC_DEF_TESTS[attr_name]
	let has_type = false
	let regex = null
	let error_level = 'error'
	let test = true
	if (attr_name.startsWith('tags.')) {
		attr_name = attr_name.replace('tags.', '')
		local_src = src.tags
	}
	if (attr_name.startsWith('token.')) {
		attr_name = attr_name.replace('token.', '')
		local_src = src.token
	}
	// console.log('test_attribute', attr_name, attr, src)
	if (!_.is(local_src[attr_name])) {
		let is_found_in_conflicts = false
		if (attr.conflicts)
			for (let i of attr.conflicts)
				if (local_src[i])
					is_found_in_conflicts = true
		if (!is_found_in_conflicts) {
			def_attr_is_complete = false
			error_attr.push(sµ.create_error(src,	sµ.error_code.NOT_FOUND_ATTR,
				`${attr_name} is needed (${dependent} dependencies)`,
				'error', 'test_integrity', {'test': 'search attribute alternative'}))
			return [def_attr_is_complete, error_attr]
		}
	} else if (attr.conflicts) {
		for (let i of attr.conflicts) {
			let conflict_lvl = src
			if (i.startsWith('tags.')) {
				i = i.replace('tags.', '')
				conflict_lvl = conflict_lvl.tags
			}
			if (i.startsWith('token.')) {
				i = i.replace('token.', '')
				conflict_lvl = conflict_lvl.token
			}
			if (conflict_lvl[i]) {
				def_attr_is_complete = false
				error_attr.push(sµ.create_error(src,	sµ.error_code.CONFLICTS_ATTR,
					`${i} is in conflict with attribute ${attr_name}`,
					'error', 'test_integrity', {'test': 'search conflicts'}))
				return [def_attr_is_complete, error_attr]
			}
		}
	}
	if (attr.test) {
		if (attr.test_conflict)
			for (let i of attr.test_conflict)
				if (src[i])
					test = false
		if (test)
			for (const type of attr.test)
				if (slow && type === 'url') {
					if (!test_re(local_src, attr_name, _.valid_HTTP_URL_RE))
						return [def_attr_is_complete, error_attr]
					test_url(local_src, local_src[attr_name], attr_name, attr.url_code)
				} else if (type === 'oneofvalues') {
					has_type = true
					if (!Array.isArray(local_src[attr_name]))
						local_src[attr_name] = [local_src[attr_name]]
					for (let a of local_src[attr_name])
						has_type = has_type && attr.test_oneofvalues.includes(a)
					if (!has_type) {
						def_attr_is_complete = false
						if (attr.test_level) {
							def_attr_is_complete = true
							error_level = attr.test_level
						}
						error_attr.push(sµ.create_error(src,	sµ.error_code.BAD_VALUE,
							`${attr_name} source without correct value : ${local_src[attr_name]}`,
							error_level, 'test_integrity', {'test': 'oneofvalues', 'attr': attr_name}))
						return [def_attr_is_complete, error_attr]
					}
				} else if (type === 'values') {
					if (attr.test_values !== local_src[attr_name]) {
						def_attr_is_complete = false
						if (attr.test_level) {
							def_attr_is_complete = true
							error_level = attr.test_level
						}
						error_attr.push(sµ.create_error(src,	sµ.error_code.BAD_VALUE,
							`source without correct ${attr_name} : ${local_src.tags.name}`,
							error_level, 'test_integrity', {'test': 'values', 'attr': attr_name}))
						return [def_attr_is_complete, error_attr]
					}
				} else if (type === 're' &&
					!(attr_name === 'search_url' && src.method && src.method === 'POST') &&
					!(attr_name === 'search_url_web' && src.type && src.type === 'JSON')
				) {
					regex = new RegExp(attr.test_re)
					if (test_re(local_src, attr_name, regex))
						return [def_attr_is_complete, error_attr]
				}	else if (type === 'inSource') {
					if (!sources[local_src[attr_name]]) {
						def_attr_is_complete = false
						if (attr.test_level) {
							def_attr_is_complete = true
							error_level = attr.test_level
						}
						error_attr.push(sµ.create_error(src,	sµ.error_code.FATHER_NOT_FOUND,
							`${attr_name} is found but the value is not in sources : ${local_src[attr_name]}`,
							error_level, 'test_integrity', {'test': 'inSource', 'attr': attr_name}))
						return [def_attr_is_complete, error_attr]
					}
				}
	}
	if (attr.dependencies)
		for (let j of attr.dependencies)
			test_attribute(sources, src, j, slow, attr_name)
	return [def_attr_is_complete, error_attr]
}

/**
 * Searches in the given source URL for RSS feeds
 * @param {string} src_url The url of the source
 * @param {string} src_name The name of the source
 * @param {Object} src_def_tests The test_integrity.json build Object
 * @returns {Array} Who contain the warn if an rss search exists
 */
export async function search_rss(src, src_url, src_name) {
	let error = []
	const header = new Headers()
	header.append('Content-Type', 'application/rss+xml')
	const init = {
		method: 'GET',
		headers: header,
		mode: 'cors',
		cache: 'default'
	}
	let res
	try {
		for (const i of SRC_DEF_TESTS.rss_url) {
			res = fetch(src_url + i, init).then(
				function (response) {
					if (response.ok) {
						let contentType = response.headers.get('content-type')
						if (contentType && contentType.indexOf('application/rss+xml') !== -1)
							error.push(sµ.create_error(src,	sµ.error_code.RSS_POSSIBLE,
								`${src_url + i} can be xml type`,
								'warn', 'test_integrity', {'test': 'search rss'}))
					}
				})
			await res
		}
	} catch (exc) {
		console.log(exc, 'RSS', src_name)
	}
	return error
}
/**
 * Contains the already encountered source names.
 * @memberof test
 * @type {Object}
 */
let reserved_names = {}
/**
 * reserved_names content is dup and replace by an empty Object.
 */
export function clear_reserved_names() {
	reserved_names = {}
}
/**
 * Fills the reserved name list with the given source names
 * @param {Object} src A source object (intended to contain all the known sources)
 */
export function load_src_reserved_names(source_objs) {
	for (const [k, n] of Object.entries(source_objs))
		reserved_names[n.tags.name] = k
}
/**
 * Checks if the name has already been found in the tested sources
 * @param {string} name The name of the source to test
 * @returns {boolean}
 */
export function is_src_name_uniq(name) {
	return typeof reserved_names[name] === 'undefined'
}
/**
 * Tests if the given source is correct, given src_def_tests.
 * @memberof test
 * @param {Object} sources - The sources.json built object
 * @param {Object} src - The uniq source object to test
 * @param {Object} src_def_tests - The test_integrity.json built object
 * @param {Function} call_back - A callback function to run against the result
 * classic
 * @returns {Array} Boolean validity of the sources and error array
 * [True, [[error, name, level], ...]]
 */
const deprecated = ['r_dt_re', 'xml_type', 'h_title', 'headline_url']
const no_css = ['search_url_web']  // when the source is not a CSS-based one
const res_base = ['r_h1', 'r_url', 'r_dt']
const results = ['results']
export async function test_src_integrity(sources, src_key, call_back=undefined, slow=false) {
	// let start = new Date()
	let src = sources[src_key]
	let errors = []
	let def_src_is_complete = true
	let needed = SRC_DEF_TESTS.base
	let type = 'GLOBAL or '
	let known_src = {}
	for (let i of deprecated)
		if (src[i]) {
			def_src_is_complete = true
			errors.push([`${i} is deprecated`, src.tags.name,	'warn'])
		}
	if (src.type === 'XML'){
		type = type.concat('XML')
		needed = needed.concat(no_css)
	} else if (src.type === 'JSON') {
		type = type.concat('JSON')
		needed = needed.concat(no_css)
		needed = needed.concat(res_base)
		needed = needed.concat(results)
		needed.push('type')
	} else {
		type = type.concat('CSS')
		needed = needed.concat(res_base)
		needed = needed.concat(results)
		if (src.r_dt_attr && !_.is_in(['datetime', 'timestamp'], src.r_dt_attr))
			needed.push('tags.tz')
	}
	if (slow && src.type !== 'XML') {
		errors = errors.concat(await search_rss(src, src_key, src.tags.name))
	}
	if (src.method === 'POST') {
		type = type.concat(' POST')
		needed = needed.concat(['method', 'body'])
	}
	if (src.search_ctype) {
		needed = needed.concat(['method'])
	}
	for (let i of needed)
		try {
			let [def_attr_is_complete, attr_errors] = test_attribute(sources, src, i, slow, type)
			def_src_is_complete = def_src_is_complete && def_attr_is_complete
			errors = errors.concat(attr_errors)
		} catch (exec) {
			console.error(i, exec)
		}
	for (let k of Object.keys(src))
		if (!sµ.SRC_FORMAT_ORDER.includes(k.replace(
			/_xpath$|_re$|_attr$|_fmt_\d+$|_tpl$|_com$/, '')))
			errors.push(sµ.create_error(src,	sµ.error_code.SUPERFLUOUS_ATTR,
				`${src.tags.name} unknown key ${k}`, 'warn', 'test_integrity')
			)
	if (!known_src[src.tags.name])
		known_src[src.tags.name] = src_key
	if (known_src[src.tags.name] !== src_key) {
		def_src_is_complete = false
		errors.push(sµ.create_error(src,	sµ.error_code.NAME_ALREADY_TAKEN,
			`${src.tags.name} is already used`, 'error', 'test_integrity')
		)
	}
	// console.log('tested in ', new Date() - start, ' i'm inevitable')
	if (typeof call_back === 'function')
		call_back([def_src_is_complete, errors])
	else
		return [def_src_is_complete, errors]
}
