// SPDX-FileName: ./mp_utils.js
// SPDX-FileCopyrightText: 2017-2024 Simon Descarpentries <simon /\ acoeuro [] com>
// SPDX-License-Identifier: GPL-3.0-only
import * as _ from './js_utils.js'
import * as µ from './BOM_utils.js'
import * as xµ from './webext_utils.js'
import { build_src } from './source_utils.js'

/**
 * @module mp_utils
 */
/**
 * Returns the actual themes.
 * @async
 * @returns The wanted theme
 */
export async function get_wanted_theme() {
	return await get_theme() || (µ.isDarkMode() && 'dark' || 'light')
}
/**
 * Sets the theme wanted.
 * @async
 */
export async function set_theme() { if (await get_wanted_theme() === 'light') set_light_mode() }
/**
 * Sets the ligth mode color scheme.
 */
export function set_light_mode() {
	let html = document.getElementsByTagName('html')[0]
	html.style.cssText += '--mp-turquoise-background: var(--turquoise)'
	html.style.cssText += '--mp-background: var(--light-normal-background)'
	html.style.cssText += '--mp-foreground: black'
	html.style.cssText += '--mp-frame-background: var(--light-frame-background)'
	html.style.cssText += '--mp-a-color: var(--dark-turquoise)'
	html.style.cssText += '--mp-gray: var(--dark-gray)'
	let imgs = document.querySelectorAll('img[src$="_dark.svg"]')
	for (let img of imgs)
		img.src = img.src.replace('_dark', '')
}
/**
 * Gives the language we need to use.
 * @async
 * @returns The language wanted (navigator or stored)
 */
export async function get_wanted_locale() {
	return await get_stored_locale() || µ.get_browser_locale()
}
/** Gets the browser stored data for entry 'locale'.
 * @returns The local browser data */
export const get_stored_locale = async () => await xµ.get_stored('locale', '')
/** Get the browser data for entry 'tz'.
 * @returns The tz browser data */
export const get_tz = async() => await xµ.get_stored('tz',
	(new Date().getTimezoneOffset()).toString())
/** Gets the browser data for entry 'theme'.
 * @returns The theme browser data */
export const get_theme = async() => await xµ.get_stored('dark_background', '')
/** Gets the browser data for entry 'live_search_reload'.
 * @returns If the search need to be reload after sentence modification */
export const get_live_search_reload = async() => await xµ.get_stored('live_search_reload',false)
/** Gets the browser data for entry 'sentence_search'.
 * @returns Search in the sources in one word or many word */
export const get_sentence_search =    async() => await xµ.get_stored('sentence_search', true)
/** Gets the browser data for entry 'undup_results'.
 * @returns If we can have duplicate result */
export const get_undup_results =      async() => await xµ.get_stored('undup_results', true)
/** Gets the browser data for entry 'load_photos'
 * @returns If we want picture to be loaded */
export const get_load_photos =        async() => await xµ.get_stored('load_photos', true)
/** Gets the browser data for entry 'max_res_by_src'.
 * @returns The maximum of response for a source */
export const get_max_res_by_src =     async() => await xµ.get_stored('max_res_by_src', 20)
/** Gets the browser data for entry 'search_timeout'.
 * @returns The search max time before abort */
export const get_search_timeout =     async() => await xµ.get_stored('search_timeout', 90)
export const get_child_mode =         async() => await xµ.get_stored('child_mode', false)
/** Gets the browser data for entry 'news_loading'.
 * @returns If we want to load the news */
export const get_news_loading =       async() => await xµ.get_stored('news_loading', false)
/** Gets the browser data for entry 'live_news_reload'.
 * @returns If we reload if the filters change */
export const get_live_news_reload =   async() => await xµ.get_stored('live_news_reload', false)
/** Gets the browser data for entry 'keep_host_perm'.
 * @returns We keep the permission for cors */
export const get_keep_host_perm =     async() => await xµ.get_stored('keep_host_perm', true)
/** Gets the browser data for entry 'max_news_loading'.
 * @returns The number of maximum result for news */
export const get_max_news_loading =   async() => await xµ.get_stored('max_news_loading', 10)
export const get_max_news_by_src =    async() => await xµ.get_stored('max_news_by_src', 10)
/** Gets the browser data for entry 'news_page_size'.
 * @returns The number of result in the news page */
export const get_news_page_size =     async() => await xµ.get_stored('news_page_size', 8)
export const resolved_search_hosts =  async() => await xµ.get_stored('resolved_search_hosts',{})
export const get_custom_src =  				async() => await xµ.get_stored('custom_src', {})
export const get_manifest = 					async() => await xµ.get_manifest()
export const get_filters =  					async() => await xµ.get_stored('filters', {})
/**
 * Remove all CORS permissions
 */
export async function drop_host_perm () { xµ.browser_drop_host_perm() }
/**
 * Stores the given value to persistant storage
 * @param {string} key The id to retrieve the value later
 * @param {string} val The value to store
 */
export function to_storage(key, val) { return xµ.try_store(key, val) }
/*
 *
 */
export function get_storage() { return xµ.get_storage() }
/*
 *
 */
export function del_storage(key) { return xµ.del_storage(key) }
/**
 * Tries to create a Meta-Press.es decorated user notification
 * @param {string} title The title of the notification
 * @param {string} body The text body of the notification
 */
export function notify_user(title, body) {
	xµ.generate_notification(title, {
		body: body,
		icon: '/img/favicon-metapress-v2.png',
		image: '/img/logo-metapress_sq.svg',
		badge: '/img/favicon-metapress-v2.png',
		actions: []
	})
}
/**
 * Gets search and headlines current host perm
 * @param {Array} sel_src The currently selected sources
 * @param {Object} src_objs The built sources
 * @returns {Array} anonymous [perm, cur_host_perm] : all the webext permissions, the current
 * host permissions
 */
async function get_current_host_perm(sel_src, src_objs) {
	let perm = await xµ.get_all_permissions()
	let cur_host_perm  = get_current_news_hosts(sel_src, src_objs)
	cur_host_perm = cur_host_perm.concat(get_current_search_hosts(sel_src, src_objs))
	cur_host_perm = _.remove_duplicate_in_array(cur_host_perm)
	return [perm, cur_host_perm]
}
/**
 * Checks if the CORS permission for selected sources exist.
 * @param {Array} sel_src The URL of sources to check
 * @param {Object} src_objs The built sources
 * @returns {boolean} Permission existence
 */
export async function check_host_perm(sel_src, src_objs) {
	const [perm, cur_host_perm] = await get_current_host_perm(sel_src, src_objs)
	for(let val of cur_host_perm) {
		if (!perm.origins.includes(val))
			return false
	}
	return true
}
/**
 * Gets the number of missing CORS permissions
 * @param {Array} sel_src The URL of sources to check
 * @param {Object} src_objs The built sources
 * @returns {number} Number of needed permission(s)
 */
export async function get_nb_needed_host_perm(sel_src, src_objs) {
	let nb_needed = 0
	let needed = []
	const [perm, cur_host_perm] = await get_current_host_perm(sel_src, src_objs)
	for(let val of cur_host_perm) {
		if (!perm.origins.includes(val)) {
			nb_needed++
			needed.push(val)
		}
	}
	return {'not_perm': nb_needed, 'names': needed}
}
/**
 * Gets the search URL for the current_source_selection.
 * @param {Array} current_source_selection The source URLs
 * @param {Object} source_objs The built sources
 * @returns {Array} The source search URLs
 */
export function get_current_search_hosts (current_source_selection, source_objs) {
	let source_hosts = []
	let s = {}
	for (let ss of current_source_selection) {
		s = source_objs[ss]
		if (!_.is(s)) continue
		const a = s.search_url
		if(a)
			if (s !== _.domain_part(a))
				source_hosts.push(`${_.domain_part(a)}/*`)
			else
				source_hosts.push(`${s}/*`)
		const b = s.search_url_web
		if(b)
			if (s !== _.domain_part(b))
				source_hosts.push(`${_.domain_part(b)}/*`)
		const redirs = s.redir_url
		if (redirs)
			if (Array.isArray(redirs))
				for (const i of redirs)
					source_hosts.push(`${i}/*`)
			else
				source_hosts.push(`${_.domain_part(redirs)}/*`)
	}
	return source_hosts
}
/**
 * Gets the headline URLs for the current_source_selection.
 * @param {Array} current_source_selection The sources URL
 * @param {Object} source_objs The built sources
 * @returns {Array} The headline URLs
 */
export function get_current_news_hosts (current_source_selection, source_objs) {
	let source_hosts = []
	let s = {}
	for (let ss of current_source_selection) {
		s = source_objs[ss]
		if (!_.is(s)) continue
		let s_hurl = s.news_rss_url
		if(s_hurl && s_hurl !== 'https://www.custom-source.eu'){
			source_hosts.push(`${_.domain_part(s_hurl)}/*`) }
	}
	return source_hosts
}
/**
 * Requests CORS permission for sources
 * @param {Object} sources The built sources
 */
export function request_sources_perm (src_keys, source_objs) {
	let s_surl = get_current_search_hosts(src_keys, source_objs)
	let perm = {origins: new Set(s_surl)}
	let s_hurl = get_current_news_hosts(src_keys, source_objs)
	for (let sh of s_hurl)
		perm.origins.add(sh)
	perm.origins = Array.from(perm.origins)
	return xµ.request_browser_origin_perm(perm)
}
/**
 * Requests the CORS permission for the given URLs.
 * @param {Array} cur_src_hosts The URLs to get the permission for
 * @returns {Promise} The response contains a boolean value
 */
export function request_permissions(cur_src_hosts) {
	return xµ.request_browser_origin_perm({origins: cur_src_hosts})
}
/*
 *
 */
export function request_one_host_perm(host) {
	return xµ.request_browser_origin_perm({origins: [`${host}/*`]})
}
/**
 * Creates an img HTML tag in a string filled with the given args.
 * @param {string} src The image url
 * @param {string} alt The image alt
 * @param {string} title The image title
 * @returns {string} The img balise filled
 */
export function img_tag(src, alt, title) {
	return `<img src="${encodeURI(src)}" alt="${alt}" title="${title}"/>`
}
/**
 * Fills URL with Meta-Press.es specific parameters.
 * @param {URL} url The url to fill
 * @param {*} mp_i18n The translation module (gettext_html_auto)
 * @returns {URL} The filled url
 */
export async function set_text_params(url, mp_i18n) {
	let params = ''
	let list_p = {
		'q': '',
		'src_type': `${mp_i18n.gettext('Type:')} `,
		'lang': `${mp_i18n.gettext('Language:')} `,
		'res_type': `${mp_i18n.gettext('Result type:')} `,
		'themes': `${mp_i18n.gettext('Themes:')} `,
		'tech': `${mp_i18n.gettext('Technical criterion:')} `,
		'country': `${mp_i18n.gettext('Country:')} `,
	}
	for(let i of Object.keys(list_p)) {
		if(url.searchParams.get(i) && url.searchParams.get(i) !== '') {
			if (i === 'q') {list_p[i] += url.searchParams.get(i)}
			else {list_p[i] += url.searchParams.getAll(i).join(', ')}
		} else {
			if (i === 'q') {list_p[i] = mp_i18n.gettext('No search terms')}
			else {list_p[i] = ''}
		}
		if(list_p[i] !== '') {
			if(i !== 'q') {params += list_p[i] + '\n'}
		}
	}
	return [list_p, params]
}
/*
 *
 */
export async function dns_resolve(host) { return await xµ.dns_resolve(host) }
/*
 *
 */
export async function try_fetch (url, method='GET', body=null) {
	/* if (!await request_one_host_perm(url)) {  // async call cancels user-originated actions
		console.warning('Refused permission')
		return {ok:false}
	}*/
	try {
		return await fetch(url, {
			method: method,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded'},
			body: body
		})
	} catch (exc) {
		console.warn(exc)
		return {ok:false}
	}
}
/**
 *
 */
export async function get_built_src() {
	if (typeof(browser) !== 'undefined') {
		// console.info("xµ.exec_in_background('bg_get_built_src')")
		return await xµ.exec_in_background('bg_get_built_src')
	} else {
		// console.info('No browser')
		return await build_src()
	}
}
export async function is_need_reload_src(dt_src_got) {
	return await xµ.exec_in_background('bg_is_need_reload_src', [dt_src_got])
}
export async function update_need_reload_src() {
	return await xµ.exec_in_background('bg_update_need_reload_src')
}
export function export_filename(mp_version, file_type) {
	return `${_.ndt_human_readable()}_meta-press.es_v${mp_version}.${file_type}`
}
